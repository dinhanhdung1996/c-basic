import java.util.*;
import java.io.*;



class Student implements Serializable
{
    private int no;
    private String SID;
    private String fullName;
    private String phoneNumber;
    private double score;


    public Student()
    {
	no = 0;
	SID = new String("");
	fullName = new String("");
	phoneNumber = new String("");
	score = 0.0;
    }
    public void setSID(String SID)
    {
	this.SID = SID;
    }

    public void setFullName(String fullName)
    {
	this.fullName = fullName;
    }


    public void setPhoneNumber(String phoneNumber)
    {
	this.phoneNumber = phoneNumber;
    }

    public void setScore(double score)
    {
	this.score = score;
    }


    public void setNo(int no)
    {
	this.no = no;
    }


    public int getNo()
    {
	return no;
    }

    public String getSID()
    {
	return SID;
    }

    public String getFullName()
    {
	return fullName;
    }

    public String getPhoneNumber()
    {
	return phoneNumber;
    }


    public double getScore()
    {
	return score;
    }


    public String toString()
    {
	return no+"\t"+ SID + "\t" + fullName + "\t\t" + phoneNumber + "\t\t" + score;
    }

}




    
