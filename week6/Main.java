import javafx.application.Application;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.*;
import javafx.geometry.*;


public class Main extends Application{

    private StudentList newObj;
    private Student []newList;


    public static void main(String []args)
    {
	launch(args);
    }

    private  Label label[][];
    private TextField text[];
    public void start(Stage primaryStage) throws Exception{

	label = new Label[100][7];
	text = new TextField[100];
	Stage window = primaryStage;


	newObj = new StudentList();
	newObj.getTextData("studentCore.txt");
	newList = newObj.getStudent();

	GridPane grid = new GridPane();
	grid.setPadding(new Insets(10, 10, 10, 10));
	grid.setVgap(10);
	grid.setHgap(10);

	Label label1 = new Label("No");
	GridPane.setConstraints(label1, 0, 0);
	Label label2 = new Label("Student ID");
	GridPane.setConstraints(label2, 1, 0);
	Label label3 = new Label("Full Name");
	GridPane.setConstraints(label3, 2, 0);
	Label label4 = new Label("Phone number");
	GridPane.setConstraints(label4, 3, 0);
	grid.getChildren().addAll(label1, label2, label3, label4);

	int i  = 0;
	while(newList[i] != null)
	    {
		label[i][1] = new Label("" + newList[i].getNo());
		GridPane.setConstraints(label[i][1], 0, i+1);
		label[i][2] = new Label(newList[i].getSID());
		GridPane.setConstraints(label[i][2], 1, i+1);
		label[i][3] = new Label(newList[i].getFullName());
		GridPane.setConstraints(label[i][3], 2, i+1);
		label[i][4] = new Label(newList[i].getPhoneNumber());
		GridPane.setConstraints(label[i][4], 3, i+1);
		text[i] = new TextField();
		text[i].setPromptText("Input the score of this student" + newList[i].getFullName());
		GridPane.setConstraints(text[i], 4, i+1);
		grid.getChildren().addAll(label[i][1], label[i][2], label[i][3], label[i][4], text[i]);
		i++;
	    };

	Button button = new Button("done");
	GridPane.setConstraints(button, 5, i++);
	button.setOnAction( e ->
			    {
				int a = 0;
				while(newList[a] != null)
				    {
					//System.out.println(a);
					//	System.out.println("error");
					if(text[a].getText() == null || text[a].getText().trim().equals("")){
					    a++;
					    continue;
					    //  a++;
					};
					//System.out.println("error2");
					
					newList[a].setScore(Double.parseDouble(text[a].getText()));
					//System.out.println("erro3");
					a++;
				    };
				newObj.saveTextData("studentDB.txt");
				newObj.saveBinData("studentDB.dat");
				
			    }
			    );
	grid.getChildren().add(button);

	Scene scene = new Scene(grid, 1000, 1000);
	window.setScene(scene);
	window.show();
    }
}

	
		
	
