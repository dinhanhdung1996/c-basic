import java.util.*;
import java.io.*;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

class StudentList{

    private Student []student = new Student[100];

    public void getTextData(String fileName)
    {

	String buffer = new String("");
	String tokens = new String("");
	String s = new String("\n\t");
	StringTokenizer strtok;

	try{
	Scanner scanner = new Scanner(new File(fileName));
	int i = 0;
	if(scanner.hasNext())
	    {
		scanner.nextLine();
	    };
	int check = 0;
	while(scanner.hasNext() )
	    {
		student[i] = new Student();
		buffer = scanner.nextLine();
		
		strtok = new StringTokenizer(buffer);
		tokens = strtok.nextToken(s);
		student[i].setNo(Integer.parseInt(tokens));
		tokens = strtok.nextToken(s);
		student[i].setSID(tokens);
		tokens = strtok.nextToken(s);
		student[i].setFullName(tokens);
		tokens = strtok.nextToken(s);
		student[i].setPhoneNumber(tokens);
		if(strtok.hasMoreTokens())
		    {
			tokens = strtok.nextToken(s);
			student[i].setScore(Double.parseDouble(tokens));
		    }
		else
		    {
		        check = 1;
		    };
		i++;
	    }
	if(check == 1)
	    {
		Alert newAlert = new Alert(AlertType.INFORMATION);
		newAlert.setTitle("Warning");
		newAlert.setHeaderText("Information");
		newAlert.setContentText("file is mssing some information\nwe will still save it into program");
		newAlert.showAndWait();
	    };
	scanner.close();
	}catch(Exception e)
	    {
		System.out.println("File error0");
		Alert alert =new Alert(AlertType.INFORMATION);
		alert.setTitle("Warning");
		alert.setHeaderText("File error");
		alert.setContentText("File is missing");
		alert.showAndWait();
		return;
	    };
	
	
    }


    public void setStudent(Student []student)
    {
	this.student = student;
    }



    public Student []getStudent()
    {
	return student;
    }


    public void saveBinData(String fileName)
    {
	try{

	    
	FileOutputStream fout = new FileOutputStream(fileName);
	ObjectOutputStream oos = new ObjectOutputStream(fout);


	int i = 0;
	while(student[i]!= null)
	    {
		oos.writeObject(student[i]);
		i++;
	    };

	oos.close();
	fout.close();
	}
	catch(Exception e)
	    {
		System.out.println("File error1");
		return;
	    };
    }

    public void saveTextData(String fileName)
    {

	try{
	Formatter formatter = new Formatter(fileName);

	int i = 0;
	formatter.format("%s", "No\tSID\t\tFull Name\t\tphone number\t\tScore\n");
	while(student[i] != null)
	    {
		formatter.format("%s\n", student[i]);
		i++;
	    };
	formatter.close();
	}
	catch(Exception e)
	    {
		System.out.println("File error2");
		return;
	    };
    }

}

