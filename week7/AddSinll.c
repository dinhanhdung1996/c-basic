#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "AddSinll.h"


//Initiate____________________________________________________________
node *makeNewNode(phoneManage phone)
{
  node *new = malloc(sizeof(node));
  new->data = phone;
  new->next = NULL;
  return new;
}

phoneManage makePhone()
{
  phoneManage new;
  strcpy(new.model, "");
  strcpy(new.ROM, "");
  strcpy(new.screen, "");
  new.price  = 0;
  return new;
}


phoneManage manual()
{
  phoneManage newObj;

  printf("Input the model: ");
  gets(newObj.model);
  printf("Input the memory: ");
  gets(newObj.ROM);
  printf("Input the screen: ");
  gets(newObj.screen);
  printf("Input the price: ");
  scanf("%d", &newObj.price);

  while(getchar() != '\n');


  return newObj;
}




//________________________________________________________________________
//Data read write

void setData(char *fileName)//read from text file
{
  FILE *f = fopen(fileName, "r");
  if(f== NULL)
    {
      printf("File not available \n");
      return;
    };

  char *buffer = (char*)malloc(sizeof(char) * 100);
  char s[5] = "\t\n";
  char *tokens = (char*)malloc(sizeof(char) * 100);
  phoneManage phone = makePhone();
  fgets(buffer, 100, f);
  while(fgets(buffer, 100, f) != NULL)
    {
      tokens = strtok(buffer, s);
      strcpy(phone.model, tokens);
      tokens = strtok(NULL, s);
      strcpy(phone.ROM, tokens);
     
      tokens = strtok(NULL, s);
      strcpy(phone.screen, tokens);
    
      printf("%s\n", phone.ROM);
      tokens = strtok(NULL, s);
      phone.price = atoi(tokens);
      insertCurrentPosition(phone);
     
    };
  fclose(f);
  
}


void saveData(char *fileName)//save into .dat
{
  FILE *f = fopen(fileName, "wb");


  node *temp = root;
  while(temp != NULL)
    {
     
      fwrite(&(temp->data), sizeof(phoneManage), 1, f);
      temp = temp->next;
    };
  if(root1 != NULL)
    {
      temp = root1;
      while(temp != NULL)
	{
	 
	  fwrite(&(temp->data), sizeof(phoneManage), 1, f);
	  temp = temp->next;
	};

    };

  fclose(f);
  return;
}


void readData(char *fileName) //read from .dat
{
  FILE *f = fopen(fileName, "rb");
  if(f == NULL)
    {
      printf("file not available\n");
      return;
    };

  phoneManage phone;
  while(fread(&phone, sizeof(phoneManage), 1, f) != 0)
    {
   
      insertCurrentPosition(phone);
      readNode(root);
      readNode(cur);
    };
  fclose(f);
}


//_______________________________________________________________________
//Single link list process


//Insert---------------------------------------------------
void insertAtHead(phoneManage phone)
{

  node *new = makeNewNode(phone);
  if(root == NULL)
    {
      root = new;
      cur = root;
      return;
    };

  new->next = root;
  root = new;
  cur = root;
}

void insertCurrentPosition(phoneManage phone)
{
  node *new = makeNewNode(phone);
  if(root == NULL)
    {
      insertAtHead(phone);
      return;
    };

  new->next = cur->next;
  cur->next = new;
  cur = cur->next;
}

void insertBeforePosition(phoneManage phone)
{
 
  if(root == NULL)
    {
      insertAtHead(phone);
      return;
    };

  node *cur = searchPrev();
  insertCurrentPosition(phone);
  cur = cur->next;
}


void insertAtPosition(phoneManage pos, phoneManage phone)
{

  if(root == NULL)
    {
      printf("Empty list \n");
      return;
    };
  node *new = search(pos);
  if(new == NULL)
    {
      printf("Posititon not available \n");
      return;
    };

  node *temp = cur;
  cur = new;
  cur = temp;
  insertCurrentPosition(phone);
  return;
}
//-----------------------------------------------------------
//Search------------------------------------------------

node *search(phoneManage phone)
{

  node *new = makeNewNode(phone);
  if(root== NULL)
    {
      printf("Emtpy list \n");
      return NULL;
    };

  while(new != NULL)
    {
      if(compare(new->data, phone))
	{
	  return new;
	};
      new = new->next;
    };
  return NULL;
}

node *searchPrev()
{
  node *new = root;
  while(new != NULL)
    {
      if(new->next == cur)return new;
      new = new->next;
    };
  return NULL;
}

node *searchModel(char *model)
{

  node *temp = root;
  while(temp != NULL)
    {
      if(strcmp((temp->data).model, model) == 0)
	{
	  return temp;
	};

      temp = temp->next;
    };
  return NULL;
}



void pointToEnd()
{
  cur = root;
  while(cur->next != NULL)
    {
      cur = cur->next;
    };

}
node *searchPrevNode(node *temp)
{
  node * new = root;
  while(new!= NULL)
    {
      if( new->next == temp)return new;
      new = new->next;
    };
  return NULL;
}


//----------------------------------------------------------
//Delte---------------------------

void deleteAtPosition(phoneManage phone)
{
  if(root == NULL)
    {
      printf("Empty list \n");
      return;
    };
  node *new = search(phone);
  if(new == NULL)
    {
      printf("Cannot perform tasks\n");
      return;
    };

  cur = new;
  node *temp = searchPrev();


  if(cur->next == NULL)
    {
      free(cur);
      cur = temp;
      return;
    };

  temp->next = cur->next;
  cur = temp;
  free(new);
  return;
  
}

void deleteAtCurrent()
{
  node *temp = cur;
  if(temp == NULL)return;
  node *temp2 = searchPrev();
  if(temp2 == NULL)
    {
      free(cur);
      return;
    };
  temp2->next = cur->next;
  cur = temp2;
  free(temp);
}

void deleteFirst()
{
  if(root == NULL)
    {
      printf("Empty list \n");
      return;
    };

  node *temp = root;
  root = root->next;
  free(temp);
}

//----------------------------------------------------

//Display----------------------------------------


void display(node *head)
{
  node *temp = head;
  if(head == NULL)
    {
      printf("Empty list\n");
    };
  while(temp != NULL)

    {
      //if(temp == NULL)break;
      readNode(temp);
      temp = temp->next;
    };

}


void readNode(node *temp) //print data
{
  if(temp == NULL)
    return;


  printf("%s\t%s\t\t%s\t%d\n", (temp->data).model, (temp->data).ROM, (temp->data).screen, (temp->data).price);

}


//Others...............................................

int compare(phoneManage phone1, phoneManage phone2)
{
  int x1 = strcmp(phone1.model, phone2.model);
  int x2 = strcmp(phone1.ROM, phone2.ROM);
  int x3 = strcmp(phone1.screen, phone2.screen);
  if(x1 == 0 && x2 == 0 && x3 == 0 && phone1.price == phone2.price)
    {
      return 1;
    };
  return 0;
}

void devideFromPosition(phoneManage phone)
{
  node *new = search(phone);
  if(new == NULL)
    {
      printf("phone model not available \n");
      return;
    };

  if(root1 == NULL)
    {
      root1 = new;
      return;
    }
  else
    {
      printf("The second list is not NULL, if you continue all data in the second list will be deleted \n");
      printf("1. yes");
      printf("2. no ");
      int choice;
      scanf("%d", &choice);
      while(getchar() != '\n');

      if(choice == 1)
	{
	  root1 = new;
	  return;
	};
      if(choice == 2)
	{
	  return;
	};
    };
  return;
}


void reverseList()
{
  pointToEnd();
  node *temp = cur;
  if(temp == NULL)
    return;
  node *temp2 = malloc(sizeof(node));
  while(temp != NULL)
    {
      if(temp == root)break;
      temp2 = searchPrevNode(temp);
      readNode(temp);
      temp->next = temp2;
      temp = temp->next;
    };
  root = cur;
      

}
      
      

