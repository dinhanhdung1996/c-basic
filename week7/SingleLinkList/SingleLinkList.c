#include <stdio.h>
#include <stdlib.h>
#include "SingleLinkList.h"
#include "dataType.h"


NodeList *makeNodeSList(ElType data)
{
  NodeList *new = malloc(sizeof(NodeList));
  new->next = NULL;
  new->data = data;
  return new;
}

SList *makeSList()
{
  SList *list = malloc(sizeof(SList));
  list->root = NULL;
  list->current = NULL;
  return list;
}


void insertAtHead(SList *list, ElType data)
{
  if(list->root == NULL)
    {
      list->root = makeNodeSList(data);
      list->current = list->root;
      return ;
    };

  NodeList *new = makeNodeSList(data);

  new->next = list->root;
  list->root = new;
}


void insertCurrentPosition(SList *list, ElType data)
{
  if(list->root == NULL)
    {
      insertAtHead(list, data);
      return;
    };
  NodeList *new = makeNodeSList(data);
  (list->current)->next = new;
  list->current = new;
}

void insertBeforeCurrent(SList *list, ElType data)
{
  if(list->root == NULL){
    insertAtHead(list, data);
    return ;};
  NodeList *new = makeNodeSList(data);
  NodeList *temp = list->root;
  if(temp == list->current)
    {
      insertAtHead(list, data);
      return;
    };
  while(temp != NULL)
    {
      if(temp->next == (list->current) )
	{
	  new->next = temp->next;
	  temp->next = new;
	  return;
	};
      temp = temp->next;
    };      
}



NodeList *searchNodeList(SList *list, ElType data, int (*compare)(ElType, ElType))
{
  if((list->root)== NULL) return NULL;

  NodeList *temp = list->root;
  while(temp != NULL)
    {
      if( compare(temp->data, data) == 0) return temp;
      temp = temp->next;
    };

  return NULL;
}

NodeList *searchPrevCurrent(SList *list)
{
  if((list->root) == NULL) return NULL;
  NodeList *temp = list->root;
  while(temp != NULL)
    {
      if( temp->next == list->current) return temp;
      temp = temp->next;
    };
  return NULL;
}


NodeList *searchPrevNode(SList *list, NodeList *node)
{
  if(list->root == NULL) return NULL;

  NodeList *temp = list->root;

  while(temp != NULL)
    {
      if(temp->next == node) return temp;
      temp = temp->next;
    };
  return NULL;
}

//-----------

void traversal(SList *list, void (*function)(ElType)){
  if((list->root) == NULL) return ;

  NodeList *temp = list->root;

  while(temp != NULL)
    {
      function(temp->data);
      temp = temp->next;
    };
}

ElType deleteAtHead(SList *list, ElType (*makeNullElType)())
{
  if(list->root == NULL)
    {
      printf("Empty list\n");
      return makeNullElType();
    };

  NodeList *temp = list->root;
  list->root = temp->next;
  ElType a = temp->data;
  free((void*)temp);
  return a;
}
  


ElType deleteAtPosition(SList *list, ElType data, int (*compare)(ElType, ElType), ElType (*makeNullElType)())
{
  if(list->root == NULL){

    printf("Empty list\n");
    return makeNullElType();
  };

  NodeList *temp = searchNodeList(list, data, compare);
  if(temp == NULL) return makeNullElType();

  NodeList *temp2 = searchPrevNode(list, temp);
  if(temp2 == NULL){
   return deleteAtHead(list, makeNullElType);
   
  };
  temp2->next = temp->next;
  ElType a= temp->data;
  free(temp);
  return a;
}




ElType deleteCurrent(SList *list, ElType (*makeNullElType)())
{
  if(list->root== NULL)
    {
      printf("Empty list\n");
      return makeNullElType();
    };

  NodeList *temp = list->current;
  list->current = searchPrevCurrent(list);
  if(list->current == NULL){
    return deleteAtHead(list, makeNullElType);
  };
  ElType a = temp->data;
  (list->current)->next = NULL;
  free((void*)temp);
  return a;
}
  






      
