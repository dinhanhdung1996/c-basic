#ifndef _DATATYPE_H_
#define _DATATYPE_H_

typedef struct phoneAddress{
  char name[40];
  char tel[30];
  char email[30];
}phoneAddress;

typedef phoneAddress ElType;
  
typedef struct NodeList{
  ElType data;
  struct NodeList *next;}NodeList;

typedef struct SList{
  NodeList *root;
  NodeList *current;}SList;
#endif
