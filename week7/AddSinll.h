#ifndef ADDSINLL_H_
#define ADDSINLL_H_


//create
typedef struct phoneManage
{

  char screen[5];//Why screen set after wrong will lead to read wrong data
  char model[40];
  char ROM[10];
  int price;
}phoneManage;


typedef struct node
{
  phoneManage data;
  struct node *next;
}node;

node *root;
node *cur;
node *root1;
node *cur1;


//Initiate____________________________________________________________
node *makeNewNode(phoneManage phone);//make a new ssl node

phoneManage makePhone(); //make a NULL phone

phoneManage manual(); //manual get Data
//________________________________________________________________________
//Data read write
void setData(char *fileName);//read from text file

void saveData(char *fileName);//save into .dat

void readData(char *fileName); //read from .dat
//_______________________________________________________________________
//Single link list process


//Insert---------------------------------------------------
void insertAtHead(phoneManage phone);

void insertCurrentPosition(phoneManage phone);

void insertBeforePosition(phoneManage phone);

void insertAtPosition(phoneManage pos, phoneManage phone);
//-----------------------------------------------------------
//Search------------------------------------------------
node *search(phoneManage phone);

node *searchModel(char *model);

void pointToEnd();

node *searchPrev();

node *searchPrevNode(node *temp);
//----------------------------------------------------------
//Delte---------------------------
void deleteAtPosition(phoneManage phone);

void deleteAtCurrent();

void deleteFirst();
//----------------------------------------------------

//Display----------------------------------------
void display(node *head);

void readNode(node *temp); //print data
//Others...............................................
int compare(phoneManage phone1, phoneManage phone2);

void devideFromPosition(phoneManage phone); //Wrong

void reverseList();
//---------------------------------------------------------------------
///__________________________________________________________________________-
#endif
