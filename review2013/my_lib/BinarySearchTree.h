#ifndef _BINARYSEARCHTREE_H
#define _BINARYSEARCHTREE_H

#include "dataType.h"


Tree makeNullTree();

int numCompare(int a, int b);


Tree makeRootTree(ElType data);

void insertNode(Tree *tree, ElType data, int (*compare)(ElType, ElType));

Tree searchNode(Tree tree, ElType data, int (*compare)(ElType, ElType));

ElType deleteMin(Tree *root);

void deleteNode(ElType x, Tree *Root, int (*compare)(ElType, ElType));


void freeTree(Tree tree);

void preOrderTraversal(Tree tree, void (*function)(Tree));

void inOrderTraversal(Tree tree, void(*function)(Tree) );

void postOrderTraversal(Tree tree, void(*function)(Tree));

void printNode(Tree tree);

#endif
  

      
      

  
