#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dataType.h"
#include "soccerResult.h"
#include "BinarySearchTree.h"



int main()
{

  Tree tree = NULL;
  int choice;
  int X = 0;
  do
    {
      printf("Menu \n");
      printf("1. Get data to tree \n");
      printf("2. Result\n");
      printf("3. Search club \n");
      printf("4. Get down\n");
      printf("5. Output file \n");

      printf("Your choice: ");
      scanf("%d", &choice);

      while(getchar() != '\n');


      if(choice == 1)
	{
	  getData("BongDa.txt", &tree);
	  printInfo(tree);
	  printf("done\n");
	};
      if(choice == 2)
	{
	  preOrderTraversal(tree, &printInfo);
	};

      if(choice == 3)
	{
	  printf("Input the name of the club : ");
	  char *club = (char*)malloc(sizeof(char) * 100);
	  gets(club);

	  Tree getTree  = searchNode(tree, makeClub(club), &compareName);
	  if(getTree == NULL)printf("No club available\n");
	  else
	  printf("Score of %s team is: %d\n",(getTree->key).name, (getTree->key).score);
	};


      if(choice == 4)
	{
	  int score;
	  printf("Input X: ");
	  scanf("%d", &score);
	  while(getchar() != '\n');
	  preOrder(&tree, score);
	  
	};

      if(choice == 5)
	{
	  clearFile("KetQua.txt");
	  preOrderTraversal(tree, &outputFileInfo);
	};
    }while(choice != 6);

  return 0;
}
