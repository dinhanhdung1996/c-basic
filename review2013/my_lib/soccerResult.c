#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dataType.h"
#include "soccerResult.h"
#include "BinarySearchTree.h"


int compareName(FootballClub f1, FootballClub f2)
{
  return (int)strcmp(f1.name, f2.name);
}

void printInfo(Tree tree)
{
  printf("%s - %d\n", (tree->key).name, (tree->key).score);
}


void getData(char *fileName, Tree *tree){

  FILE *f = fopen(fileName, "r");

  char *s = "\t\n";
  char *token = (char*)malloc(sizeof(char)* 100);
  char *buffer = (char *)malloc(sizeof(char)* 100);

  FootballClub *fc1 = malloc(sizeof(FootballClub));
  FootballClub *fc2 = malloc(sizeof(FootballClub));
  int score1 = 0;
  int score2 = 0;
  while(fgets(buffer, 100, f) != NULL)
    {
      token = strtok(buffer, s);
      strcpy(fc1->name, token);
      token = strtok(NULL,s);
      strcpy(fc2->name, token);
      token = strtok(NULL, s);
      score1 = atoi(token);
      token = strtok(NULL, s);
      score2 = atoi(token);
      insertNode(tree, *fc1, &compareName);
      insertNode(tree, *fc2, &compareName);
      Tree tree1 = searchNode(*tree, *fc1, &compareName);
      printInfo(tree1);
      Tree tree2 = searchNode(*tree, *fc2, &compareName);
      printInfo(tree2);
      if(score1 > score2)
	{
	  (tree1->key).score +=3;
	  (tree2->key).score -=1;
	}
      
      else if(score1 < score2)
	{
	  (tree1->key).score -=1;
	  (tree2->key).score +=3;
	}
      else
	{
	  (tree1->key).score +=1;
	  (tree2->key).score +=1;
	};
    };

  printf("end\n");
  fclose(f);
}

	  


void outputFileInfo(Tree tree)
{
  FILE *f = fopen("KetQua.txt", "a");
  fprintf(f, "%s\t%d\n", (tree->key).name, (tree->key).score);
  fclose(f);
}

void clearFile(char *fileName)
{
  FILE *f = fopen(fileName, "w");

  fclose(f);
}


FootballClub makeClub(char *name){
  FootballClub new;
  strcpy(new.name, name);
  new.score = 0;
  return new;
}

  


void preOrder(Tree *tree, int x)
{
  if(((*tree)->key).score < x) deleteNode((*tree)->key, tree, &compareName);
  if((*tree)->left != NULL) preOrder(&(*tree)->left, x);
  if((*tree)->right != NULL) preOrder(&(*tree)->right, x);
}
