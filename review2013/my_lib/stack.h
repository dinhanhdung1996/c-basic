#ifndef STACK_H_INCLUDED
#define STACK_H_INCLUDED
#include "dataType.h"



NodeList *makeNode(ElType data);
Stack *makeStack();
void push(ElType data, Stack *stack);
ElType pop(Stack * stack, ElType (*nullElType)());
ElType viewTop(Stack *stack, ElType (*nullElType)());

#endif // STACK_H_INCLUDED
