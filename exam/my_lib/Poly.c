#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dataType.h"
#include "SingleLinkList.h"
#include "Poly.h"


Poly nullPoly()
{
  Poly poly;
  poly.coef = -1;
  poly.exp = 0;
  return poly;
}

void bufferToList(SList *list, char *buffer)
{
  char *token = (char*)malloc(sizeof(char) * 100);
  char *s = " \n";
  Poly poly;
  token = strtok(buffer, s);
  if(token == NULL) return;
  poly.coef = atoi(token);
  token = strtok(NULL, s);
  if(token == NULL) return;
  poly.exp = atoi(token);
  insertCurrentPosition(list, poly);
  poly.coef = 0;
  poly.exp = 0;
  while( (token = strtok(NULL, s)) != NULL)
    {
      poly.coef = atoi(token);
      if((token = strtok(NULL, s)) != NULL)
	poly.exp = atoi(token);
      else break;
      insertCurrentPosition(list, poly);
      poly.coef = 0;
      poly.exp = 0;
    };
}

void getData(char *fileName, SList *list1, SList *list2)
{
  FILE *f = fopen(fileName, "r");
  if(f == NULL)
    {
      printf("file unavailable\n");
      return;
    };
  char *buffer = (char*)malloc(sizeof(char) * 100);

  if(fgets(buffer, 100, f) != NULL)bufferToList(list1, buffer);
  if(fgets(buffer, 100, f) != NULL)bufferToList(list2, buffer);
  fclose(f);
}

void printPoly(Poly poly)
{
  if(poly.coef >0) printf("+");
  else if(poly.coef == 0) return;
 
  if(poly.exp > 0 && poly.exp != 1){
    if(poly.coef != 1)printf("%d(X^%d)", poly.coef, poly.exp);
    else printf("X^%d", poly.exp);
  }
  else if(poly.exp == 1) printf("%dX", poly.coef);
  else printf("%d", poly.coef);
}

//void printPolyA(Poly *poly)
//  {
    //  if(poly.coef >0) printf("+");
  //  if(poly.exp > 0){
    //    
    //   printf("%d(X^%d)(%x)", poly->coef, poly->exp, poly);
    // };
  //  else printf("%d(%x)", poly->coef, poly);
  //}


int polyCompare(Poly poly1, Poly poly2)
{
  if(poly1.coef == poly2.coef && poly1.exp == poly2.exp) return 0; 
  int n = 5;
  return n;
}

int expCompare(Poly poly1, Poly poly2)
{
  if(poly1.exp == poly2.exp ) return 0;
  if(poly1.exp > poly2.exp) return 1;
  if(poly1.exp < poly2.exp) return -1;

  return 0;
}
void freeL(SList *list){}
void exchangeNode(NodeList *a, NodeList *b)
{
  Poly temp = a->data;
  a->data = b->data;
  b->data = temp;
}

void normFree(SList *list)
{
  if(list->root == NULL)
    {
      printf("Empty list\n");
      return;
    };
  NodeList *temp1 = list->root;
  NodeList *temp2 = temp1->next;

  while(temp1 != NULL)
    {
      temp2 = temp1->next;
      while(temp2 != NULL)
	{
	  int a = expCompare(temp1->data, temp2->data);
	  if(a == 0)
	    {
	      (temp1->data).coef += (temp2->data).coef;
	      NodeList *temp3 = searchPrevNode(list, temp2);
	      if(temp3 == NULL)continue;
	      temp3->next = temp2->next;
	      NodeList *temp4 = temp2;
	      temp2 = temp3;
	      free(temp4);
	    };
	  temp2 = temp2->next;
	};

      temp1 = temp1->next;
    }
}


void normOrder(SList *list)
{
  if(list->root == NULL)
    {
      printf("Empty\n");
      return;
    };

  NodeList *temp1 = list->root;
  NodeList *temp2;

  while(temp1 != NULL)
    {
      temp2 = temp1->next;
      while(temp2 != NULL)
	{
	  int a = expCompare(temp1->data, temp2->data);
	  if(a < 0)exchangeNode(temp1, temp2);
	  temp2 = temp2->next;
	};
      temp1 = temp1->next;
    }
}	  

void printPolyA(Poly *a)
{
  printPoly(*a);
  printf("(%x)", a);
}


void traversalA(SList *list)
{
  if(list->root == NULL) return;
  NodeList *temp = list->root;

  while(temp != NULL)
    {
      printPoly(temp->data);
      printf("(%x)", temp);
      temp = temp->next;
    };

}


SList *plus(SList *list1, SList *list2)
{
  NodeList *temp1 = list1->root;
  NodeList *temp2 = list2->root;
  SList *R = makeSList();
  Poly poly;
  poly.coef = 0;
  poly.exp = 0;
  while(temp1 != NULL)
    {
      while(temp2 != NULL)
	{
	  printf("1\n");
	  int a= expCompare(temp1->data, temp2->data);
	  if(a == 0)
	    {
	      poly.coef = (temp1->data).coef + (temp2->data).coef;
	      poly.exp = (temp1->data).exp;
	      insertCurrentPosition(R, poly);
	
	    };
	  temp2 = temp2->next;
	};
      temp2 = list2->root;
      temp1 = temp1->next;
    };
  temp1 = list1->root;
  NodeList *temp3 = R->root;
  while(temp3 != NULL)
    {
      while(temp1 != NULL)
	{
	  printf("2\n");
	  int c = expCompare(temp1->data, temp3->data);
	  if(c != 0) insertCurrentPosition(R, temp1->data);
	  temp1 = temp1->next;
	};
      printf("4\n");
      temp1 = list1->root;
      temp3 = temp3->next;
    };

  temp3 = R->root;
  temp2 = list2->root;
  while(temp3 != NULL)
    {
      while(temp2 != NULL)
	{
	  printf("3\n");
	  int c = expCompare(temp1->data, temp3->data);
	  if(c!= 0) insertCurrentPosition(R, temp2->data);
	  temp2 = temp2->next;
	};
      temp2 = list2->root;
      temp3 = temp3->next;
    };
  return R;

}


SList *plus2(SList *list1, SList *list2)
{
  SList *R = makeSList();

  NodeList *temp = list1->root;

  while(temp != NULL)
    {
      insertCurrentPosition(R, temp->data);
      temp = temp->next;
    };

  temp = list2->root;
  while(temp != NULL)
    {
      insertCurrentPosition(R, temp->data);
      temp = temp->next;
    };
  normFree(R);
  normOrder(R);
  return R;
}


	  
      
