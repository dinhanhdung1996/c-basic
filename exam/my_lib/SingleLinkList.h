
#include "dataType.h"


NodeList *makeNodeSList(ElType data);

SList *makeSList();

void insertAtHead(SList *list, ElType data);

void insertCurrentPosition(SList *list, ElType data);

void insertBeforeCurrent(SList *list, ElType data);


NodeList *searchNodeList(SList *list, ElType data, int (*compare)(ElType, ElType)); 


NodeList *searchPrevCurrent(SList *list);

NodeList *searchPrevNode(SList *list, NodeList *node);
//-----------

void traversal(SList *list, void (*function)(ElType));

ElType deleteAtPosition(SList *list, ElType data, int (*compare)(ElType, ElType), ElType (*makeNullElType)());

ElType deleteCurrent(SList *list, ElType (*makeNullElType)());

ElType deleteAtHead(SList *list, ElType (*makeNullElType)());
