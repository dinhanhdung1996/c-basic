
#include "dataType.h"


Poly nullPoly();

void bufferToList(SList *list, char *buffer);

void getData(char *fileName, SList *list1, SList *list2);

void printPoly(Poly poly);    
  
//void printPolyA(Poly poly);

void normFree(SList *list);

void normOrder(SList *list);

void traversalA(SList *list);

SList *plus(SList *list1, SList *list2);

SList *plus2(SList *list1, SList *list2);

void freeL(SList *list);
