#ifndef _PHONEDEAL_H
#define _PHONEDEAL_H

#include "dataType.h"
#define FILE_OUT "result.txt"
#include "BinarySearchTree.h"

int emailCompare(phoneAddress phone1, phoneAddress phone2);

int emailComparePure(char *email1, char *email2);

int getData(char *fileName, phoneAddress *phone);

void printPhone(phoneAddress phone);

void printListPhone(Tree tree);

void appendData(char *fileName, phoneAddress phone);

phoneAddress newPhone(char *email);

void phoneAssign(phoneAddress *phone1, phoneAddress *phone2);

phoneAddress nullPhone();

phoneAddress makePhone(char *email);

void appendDataConst(phoneAddress phone);

void clearFile(char *fileName);
#endif

