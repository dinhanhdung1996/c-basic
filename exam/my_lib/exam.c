#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Poly.h"
#include "SingleLinkList.h"
#include "dataType.h"
#define FILE_NAME "Poly.txt"

int main()
{
  SList *P = makeSList();
  SList *Q = makeSList();
  int choice;

  do
    {
      printf("MENU\n");

      printf("1. Read data from \"Poly.txt\"\n");
      printf("2. Show data \n");
      printf("3. Normialization\n");
      printf("4. Plus 2 equation\n");
      printf("5. Exit\n");


      printf("Your chocie: ");
      scanf("%d", &choice);
      while(getchar() != '\n');

      if(choice ==1 )
	{
	  getData(FILE_NAME, P, Q);
	  
	  printf("done \n");
	  
	};

      if(choice == 2)
	{
	  printf("P = ");
	  traversal(P, &printPoly);
	  printf("\n");

	  printf("Q = ");
	  traversal(Q, &printPoly);
	  printf("\n");
	  printf("Address\n");
	  printf("P = ");
	  traversalA(P);

	  printf("\n");
	    printf("Q = ");
      	  traversalA(Q);
	    printf("\n");
	};


      if(choice == 3)
	{
	  normFree(P);
	  normOrder(P);
	  printf("P = ");
	  traversal(P, &printPoly);
	  printf("\n");
	  normFree(Q);
	  normOrder(Q);
	  printf("Q = ");
	  traversal(Q, &printPoly);
	  printf("\n");
	};

      if(choice == 4)
	{
	  SList *R = plus2(P, Q);
	  printf("R = ");
	  traversal(R, &printPoly);
	  printf("\n");
	};

      if(choice == 5)
	{
	  freeL(P);
	  freeL(Q);
	};

    }while(choice != 5);

  return 0;
}
