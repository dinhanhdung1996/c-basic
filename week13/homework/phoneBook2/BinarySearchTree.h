
typedef struct phoneAddress{
  char name[30];
  char tel[20];
  char email[40];
}phoneAddress;


typedef phoneAddress ElType;

typedef struct Node{
  ElType key;
  struct Node *left, *right;
}Node;


typedef Node *Tree;


int emailCompare(phoneAddress phone1, phoneAddress phone2);

int emailComparePure(char *email1, char *email2);

int getData(char *fileName, phoneAddress *phone);

void printPhone(phoneAddress phone);

void printListPhone(Tree tree);

void appendData(char *fileName, phoneAddress phone);

phoneAddress newPhone(char *email);

void phoneAssign(phoneAddress *phone1, phoneAddress *phone2);


Tree makeNullTree();

int numCompare(int a, int b);


Tree makeRootTree(ElType data);

void insertNode(Tree *tree, ElType data, int (*compare)(ElType, ElType));

Tree searchNode(Tree tree, ElType data, int (*compare)(ElType, ElType));

ElType deleteMin(Tree *root);

void deleteNode(ElType x, Tree *Root, int (*compare)(ElType, ElType));


void freeTree(Tree tree);

void preOrderTraversal(Tree tree, void (*function)(Tree));

void inOrderTraversal(Tree tree, void(*function)(Tree) );

void postOrderTraversal(Tree tree, void(*function)(Tree));

void printNode(Tree tree);


  
  

      
      

  
