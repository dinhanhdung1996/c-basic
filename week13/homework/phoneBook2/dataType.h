#ifndef _DATATYPE_H
#define _DATATYPE_H
typedef struct phoneAddress{
  char name[30];
  char tel[20];
  char email[40];
}phoneAddress;


typedef phoneAddress ElType;

typedef struct Node{
  ElType key;
  struct Node *left, *right;
}Node;


typedef Node *Tree;

#endif
