#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "BinarySearchTree.h"

int emailCompare(phoneAddress phone1, phoneAddress phone2)
{
  printf("%s - %s\n", phone1.email, phone2.email);
  printf("%p - %p\n", phone1.email[0], phone2.email[0]);
  return strcmp(phone1.email, phone2.email);
}


int emailComparePure(char *email1, char *email2)
{
  return strcmp(email1, email2);
}

void printPhone(phoneAddress phone)
{
  printf("%s\t\t%s\t%s\n", phone.name, phone.tel, phone.email);
}

int getData(char *fileName, phoneAddress *phone)
{
  FILE *f = fopen(fileName, "r");
  if(f == NULL)
    {
      printf("File error \n");
      return 0;
    };
  
  char *buffer = (char *)malloc(sizeof(char) * 100);

  char *s = "\n\t";

  char *token = (char*)malloc(sizeof(char) * 100);

  int i = 0;
  printf("%p\n", phone[i]);
  printf("%p\n", phone[i++]);
  fgets(buffer, 100, f);
  
  while(fgets(buffer, 100, f) != NULL)
    {
      token = strtok(buffer, s);
      strcpy(phone[i].name, token);
      token = strtok(NULL, s);
      strcpy(phone[i].tel,token);
      token = strtok(NULL, s);
      strcpy(phone[i].email, token);
      printf("%p\n", phone[i]);
      printPhone(phone[i]);
      i++;
    };
  return i--;
 
}


phoneAddress newPhone(char *email)
{
  phoneAddress phone;
  strcpy(phone.email, email);
  return phone;
}


void appendData(char *fileName, phoneAddress phone)
{
  FILE *f = fopen(fileName, "a");

  fprintf(f, "%s\t%s\t%s\n", phone.name, phone.tel, phone.email);
  printPhone(phone);
  fclose(f);
}



void printListPhone(Tree tree)
{
  printPhone(tree->key);
}


void phoneAssign(phoneAddress *phone1, phoneAddress *phone2)
{
  strcpy(phone1->name, phone2->name);
  strcpy(phone1->tel, phone2->tel);
  strcpy(phone1->email, phone2->email);
}


Tree makeNullTree()
{
  Tree tree = NULL;
  return tree;
}



int numCompare(int a, int b)
{
  if(a == b) return 0;
  if(a > b)return 1;
  if(a < b) return -1;
  return 0;
}


Tree makeRootTree(ElType data)
{
  //Tree newTree = malloc(sizeof(Tree));
  // newTree = malloc(sizeof(Node) );
  Tree newTree = malloc(sizeof(Tree));
  if(newTree == NULL)
    {
      printf("Can not allocate this address \n");
    };
  newTree->left = NULL;
  newTree->right = NULL;
  newTree->key = data;
  return newTree;
}


void insertNode(Tree *tree, ElType data, int (*compare)(ElType ,  ElType))
{
  if(*tree == NULL){
   
    *tree = makeRootTree(data);
    return;};

  if(compare((*tree)->key,data)== 0)return;

  else if(compare((*tree)->key, data) > 0)
    {

      insertNode(&((*tree)->left), data, compare);
    }

  else if(compare((*tree)->key ,  data) < 0)
    {
   
      insertNode(&((*tree)->right), data, compare);
    }
  else printf("3\n");
}

Tree searchNode(Tree tree, ElType data, int (*compare)(ElType, ElType))
{
  Tree newTree;
  if(tree == NULL)return NULL;

  if(compare(data, tree->key) == 0)return tree;
  
  if(compare(data, tree->key) < 0)
    newTree = searchNode(tree->left, data, compare);

  if(compare(data, tree->key) > 0)
    newTree = searchNode(tree->right, data, compare);

  return newTree;
}


ElType deleteMin(Tree *root)
{
  ElType k;
  if((*root)->left == NULL)
    {
      k = (*root)->key;
      (*root) = (*root)->right;
      return k;
    }
  else return deleteMin(&(*root)->left);
}

void deleteNode(ElType x, Tree *Root, int (*compare)(ElType, ElType))
{
  if( (*Root) != NULL)
    {
      if( compare(x , (*Root)->key) <0) deleteNode(x, &(*Root)->left, compare);
      else if( compare(x, (*Root)->key) > 0) deleteNode(x, &(*Root)->right, compare);
      else if( ((*Root)->left == NULL) && ((*Root)->right == NULL) ) *Root = NULL;
      else if( (*Root)->left == NULL) (*Root) = (*Root)->right;

      else if( (*Root)->right == NULL) *Root = (*Root)->left;

      else ((*Root)->key = deleteMin( &(*Root)->right ));
    };
}



void freeTree(Tree tree)
{
  if(tree == NULL)return;

  freeTree(tree->left);
  freeTree(tree->right);
  free((void*)tree);
}




void preOrderTraversal(Tree tree, void (*function)(Tree))
{
  function(tree);
  if(tree->left != NULL)
    { 
      preOrderTraversal(tree->left, function);
    };
  if(tree->right != NULL)
    {
     
      preOrderTraversal(tree->right, function);
    };
}


void inOrderTraversal(Tree tree, void (*function)(Tree))
{
  if(tree->left != NULL)inOrderTraversal(tree->left, function);

  function(tree);

  if(tree->right != NULL)inOrderTraversal(tree->right, function);
}



void postOrderTraversal(Tree tree, void (*function)(Tree))
{
  printf("0\n");
  if(tree->left != NULL)postOrderTraversal(tree->left, function);
  printf("1\n");
  if(tree->right != NULL)postOrderTraversal(tree->right, function);
  function(tree);
}
