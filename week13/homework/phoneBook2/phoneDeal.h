#include "dataType.h"
//#include "BinarySearchTree.h"

int emailCompare(phoneAddress phone1, phoneAddress phone2);

int emailComparePure(char *email1, char *email2);

int getData(char *fileName, phoneAddress *phone);

void printPhone(phoneAddress phone);

void printListPhone(Tree tree);

void appendData(char *fileName, phoneAddress phone);

phoneAddress newPhone(char *email);

void phoneAssign(phoneAddress *phone1, phoneAddress *phone2);

