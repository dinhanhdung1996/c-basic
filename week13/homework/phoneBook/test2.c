#include <stdio.h>
#include <stdlib.h>
#include "BinarySearchTree.h"


void printTree(Tree tree)
{
  printf("%d\n", tree->key);
}
int main()
{
  Tree tree = NULL;


  int choice;

  do
    {
      printf("MENU \n");
      printf("1. Insert node \n");
      printf("2. Search node \n");
      printf("3. print node \n");


      printf("Your choice : ");
      scanf("%d", &choice);
      while(getchar() != '\n');

      if(choice == 1)
	{
	  printf("Input an integer: ");
	  int a;
	  scanf("%d", &a);
	  while(getchar() != '\n');

	  insertNode(&tree, a, &numCompare);
	};


      if(choice == 2)
	{
	  printf("Input an integer: ");
	  int integer;
	  scanf("%d", &integer);
	  while(getchar() != '\n');
	  Tree newTree = searchNode(tree, integer, &numCompare);
	  if(newTree == NULL) continue;
	  printf("%d\n", newTree->key);
	};

      if(choice == 3)
	{
	  preOrderTraversal(tree, &printTree);
	};


    }while( choice != 4);

  return 0;
}
	  
