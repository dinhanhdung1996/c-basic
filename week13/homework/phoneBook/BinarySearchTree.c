#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "BinarySearchTree.h"


Tree makeNullTree()
{
  Tree tree = NULL;
  return tree;
}



int numCompare(int a, int b)
{
  if(a == b) return 0;
  if(a > b)return 1;
  if(a < b) return -1;
  return 0;
}


Tree makeRootTree(ElType data)
{
  Tree newTree = malloc(sizeof(Node));
  if(newTree == NULL)
    {
      printf("Can not allocate this address \n");
    };
  newTree->left = NULL;
  newTree->right = NULL;
  newTree->key = data;
  return newTree;
}


void insertNode(Tree *tree, ElType data, int (*compare)( ElType ,  ElType))
{

  if(*tree == NULL){

    *tree = makeRootTree(data);
 
    return;};

  if((*compare)((*tree)->key,data)== 0){
 
    return;
  }
  else if((*compare)((*tree)->key, data) > 0)
    {

      insertNode(&((*tree)->left), data, compare);
    }

  else if((*compare)((*tree)->key ,  data) < 0)
    {
 
      insertNode(&((*tree)->right), data, compare);
    }
  else printf("3\n");
}

Tree searchNode(Tree tree, ElType data, int (*compare)(ElType, ElType))
{
  Tree newTree;
  if(tree == NULL)return NULL;

  if(compare(data, tree->key) == 0)return tree;
  
  if(compare(data, tree->key) < 0)
    newTree = searchNode(tree->left, data, compare);

  if(compare(data, tree->key) > 0)
    newTree = searchNode(tree->right, data, compare);

  return newTree;
}


ElType deleteMin(Tree *root)
{
  ElType k;
  if((*root)->left == NULL)
    {
      k = (*root)->key;
      (*root) = (*root)->right;
      return k;
    }
  else return deleteMin(&(*root)->left);
}

void deleteNode(ElType x, Tree *Root, int (*compare)(ElType, ElType))
{
  if( (*Root) != NULL)
    {
      if( compare(x , (*Root)->key) <0) deleteNode(x, &(*Root)->left, compare);
      else if( compare(x, (*Root)->key) > 0) deleteNode(x, &(*Root)->right, compare);
      else if( ((*Root)->left == NULL) && ((*Root)->right == NULL) ) *Root = NULL;
      else if( (*Root)->left == NULL) (*Root) = (*Root)->right;

      else if( (*Root)->right == NULL) *Root = (*Root)->left;

      else ((*Root)->key = deleteMin( &(*Root)->right ));
    };
}



void freeTree(Tree tree)
{
  if(tree == NULL)return;

  freeTree(tree->left);
  freeTree(tree->right);
  free((void*)tree);
}




void preOrderTraversal(Tree tree, void (*function)(Tree))
{
  function(tree);
  if(tree->left != NULL)
    { 
      preOrderTraversal(tree->left, function);
    };
  if(tree->right != NULL)
    {
     
      preOrderTraversal(tree->right, function);
    };
}


void inOrderTraversal(Tree tree, void (*function)(Tree))
{
  if(tree->left != NULL)inOrderTraversal(tree->left, function);

  function(tree);

  if(tree->right != NULL)inOrderTraversal(tree->right, function);
}



void postOrderTraversal(Tree tree, void (*function)(Tree))
{
  //  printf("0\n");
  if(tree->left != NULL)postOrderTraversal(tree->left, function);
  // printf("1\n");
  if(tree->right != NULL)postOrderTraversal(tree->right, function);
  function(tree);
}










  
  

      
      

  
