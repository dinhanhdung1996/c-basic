#include "dataType.h"


void swap(element *a, element *b);

void quickSort(element *list, int left, int right, int (*compare)(ElType, ElType));

int numCompare(int a, int b);
