#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dataSort.h"
#include "quickSort.h"



void swap(element *a, element *b)
{
  //printf("before: a: %p: %d \t b: %p:  %d\n", a, a->key, b, b->key);
  element * temp  = malloc(sizeof(element));
  temp->key = a->key;
  a->key= b->key;
  b->key = temp->key;
  //printf("after: a: %p : %d \t b: %p:  %d\n",a,  a->key, b, b->key);
}


int numCompare(int a, int b)
{
  if(a == b) return 0;
  if(a > b) return 1;
  if(a < b ) return -1;
  return 0;
}

void quickSort(element *list, int left, int right, int (*compare)(ElType, ElType))
{
  if(right < left) return;
  int pivot, i,j;
  i = left;
  j = right + 1;
  pivot = list[left].key;
  do
    {
      
      do ++i; while(compare(list[i].key, pivot) == -1  && i <= right);
      do --j; while(compare(list[j].key,pivot) == 1);
     
      if( i < j ) swap(&list[i], &list[j]);
      //printList(list, 10);
    }while( i < j);
  
  swap(&list[left], &list[j]);
  quickSort(list, left, j - 1 , compare);
  quickSort(list, j+1, right, compare);
}


