#include <stdio.h>
#include <stdlib.h>



typedef int ElType;

typedef struct Node{
  ElType data;
  struct Node *left;
  struct Node *right;
}Node;


typedef Node *Tree;


void makeNullTree(Tree tree){
  tree = NULL;
}


int isEmpty(Tree tree){
  return tree== NULL;
}


Node *makeNodeTree(ElType data)
{
  Node *new = malloc(sizeof(Node));
  new->data = data;
  new->left = NULL;
  new->right = NULL;
  return new;
}


Tree leftChild(Tree tree)
{
  if(tree == NULL)return NULL;
  if(tree->left == NULL)return NULL;
  return tree->left;
};


Tree rightChild(Tree tree)
{
  if(tree== NULL) return NULL;
  if(tree->right == NULL)return NULL;
  return tree->right;
}

int isLeaf(Tree tree)
{
  if(tree== NULL) return -1;

  if(tree->right == NULL && tree->left == NULL)return 1;
  else return 0;
}


int numberOfNode(Tree tree)
{
  if( isEmpty(tree) ) return 0;
  else return 1 + numberOfNode(tree->left) + numberOfNode(tree->right) ;
}



Tree createFromTwo(ElType data, Tree left, Tree right)
{
  Tree newTree = makeNodeTree(data);
  newTree->left = left;
  newTree->right = right;
  return newTree;
}



Tree addToLeftMost(Tree tree, ElType data)
{
  if(tree->left == NULL)tree->left = makeNodeTree(data);
  else addToLeftMost(tree->left, data);
}

Tree addToRightMost(Tree tree, ElType data)
{
  if(tree->right == NULL)tree->right = makeNodeTree(data);
  else addToRightMost(tree->right, data);
}



int heightTree(Tree tree)
{
  if(tree == NULL)
    return -1;

  int heightLeft = heightTree(tree->left);
  int heightRight = heightTree(tree->right);

  if(heightLeft >heightRight) return heightLeft+1;
  else return heightRight + 1;
}

void postOrderPrint(Tree tree)
{
  if(tree->left != NULL) postOrderPrint(tree->left);
  if(tree->right != NULL)postOrderPrint(tree->right);
  printf("%d\n", tree->data);
}



int main()
{
  int choice;
  Tree tree;

  do
    {
      printf("1. Input root \n");
      printf("2. Add element \n");
      printf("3. Add to left most child \n");
      printf("4. Add to right most child \n");
      printf("5. print post order \n");
      printf("6. Height tree \n");
      printf("7. Count node\n");
     


      printf("your choice: ");
      scanf("%d", &choice);
      while(getchar() != '\n');


      if(choice == 1)
	{
	  printf("Input the data \n");
	  int data;
	  scanf("%d", &data);
	  while(getchar() != '\n');

	  tree = makeNodeTree(data);
	  printf("done\n");
	};

      if(choice == 2)
	{
	};

      if(choice ==3)
	{
	  printf("Input the data : ");
	  int data;
	  scanf("%d", &data);
	  while(getchar() != '\n');

	  addToLeftMost(tree, data);

	};

      if(choice == 4)
	{
	  printf("Input the data : ");
	  int data;
	  scanf("%d", &data);
	  while(getchar() != '\n');

	  addToRightMost(tree, data);
	};

      if(choice == 5)
	{
	  postOrderPrint(tree);
	};

      if(choice == 6)
	{
	  int height = heightTree(tree);
	  printf("height of tree is %d\n", height);
	};


      if(choice == 7)
	{
	  int count = numberOfNode(tree);
	  printf("number of node is %d\n", count);
	};


    }
  while(choice != 8);

  return 0;
}
	  
  

 
