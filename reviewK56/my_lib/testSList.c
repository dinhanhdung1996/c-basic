#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dataType.h"
#include "phoneDeal.h"
#include "SingleLinkList.h"
#define  FILE_TEST "phoneBook.txt"
#define FILE_OUT "result.txt"


int main()
{

  SList *list = makeSList();
  phoneAddress *phone = (phoneAddress *)malloc(sizeof(phoneAddress) * 100);

  int choice;
  do
    {
      printf("MENU\n");
      printf("1. Get data\n");
      printf("2. Show data\n");
      printf("3. Delete a node\n");
      printf("4. search a node\n");
      printf("5. Las node \n");
      printf("6. prev last node \n");
      printf("7. write to file\n");

      printf("8. exit\n");


      printf("Your choice : ");
      scanf("%d", &choice);
      while(getchar() != '\n');


      if(choice == 1)
	{
	  int num = getData(FILE_TEST , phone);
	  int i;
	  for(i = 0; i< num; i++)
	    {
	      insertCurrentPosition(list, phone[i]);
	    };

	};

      if(choice == 2)
	{
	  traversal(list, &printPhone);
	};

      if(choice == 3)
	{
	  int choice3;

	  do
	    {
	      printf("----------------------------\n");
	      printf("1. delete at a position\n");
	      printf("2. delete last node \n");
	      printf("3. back to main menu\n");

	      printf("Your choice: ");
	      scanf("%d", &choice3);
	      while(getchar() != '\n');

	      if(choice3 == 1)
		{
		  printf("Input email address to delete: ");
		  char *email = (char *)malloc(sizeof(char) * 100);
		  gets(email);

		  ElType newPhone = deleteAtPosition(list, makePhone(email), &emailCompare, &nullPhone);
		  if(strcmp(newPhone.name, "") == 0) continue;
		  printf("delete data: ");
		  printPhone(newPhone);
		};

	      if(choice3 == 2)
		{
		  ElType newPhone = deleteCurrent(list, &nullPhone);
		  printf("delete data: ");
		  printPhone(newPhone);
		};

	    }while(choice3 != 3);

	}

      if(choice == 4)
	{
	  printf("Input email address to search: ");
	  char *email = (char*)malloc(sizeof(char) * 100);
	  gets(email);
	  NodeList *a  = searchNodeList(list, makePhone(email), &emailCompare);
	  printPhone(a->data);
	};


      if(choice == 5)
	{
	  NodeList *a = searchPrevCurrent(list);
	  printPhone(a->data);
	};

      if(choice == 7)
	{
	  clearFile(FILE_OUT);
	  traversal(list, &appendDataConst);
	}

    }while( choice != 6);

  return 0;
}


      
