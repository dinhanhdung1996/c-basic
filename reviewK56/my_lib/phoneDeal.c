#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "dataType.h"
#include "phoneDeal.h"
#include "BinarySearchTree.h"

int emailCompare(phoneAddress phone1, phoneAddress phone2)
{
  
  int c = (int)strcmp(phone1.email, phone2.email);



  return c;
}


int emailComparePure(char *email1, char *email2)
{
  return strcmp(email1, email2);
}

void printPhone(phoneAddress phone)
{
  printf("%s\t\t%s\t%s\n", phone.name, phone.tel, phone.email);
}

void printListPhone(Tree tree)
{
  printPhone(tree->key);
}

int getData(char *fileName, phoneAddress *phone)
{
  FILE *f = fopen(fileName, "r");
  if(f == NULL)
    {
      printf("File error \n");
      return 0;
    };
  
  char *buffer = (char *)malloc(sizeof(char) * 100);

  char *s = "\n\t";

  char *token = (char*)malloc(sizeof(char) * 100);

  int i = 0;

  fgets(buffer, 100, f);
  
  while(fgets(buffer, 100, f) != NULL)
    {
      token = strtok(buffer, s);
      strcpy(phone[i].name, token);
      token = strtok(NULL, s);
      strcpy(phone[i].tel,token);
      token = strtok(NULL, s);
      strcpy(phone[i].email, token);
      i++;
    };
  return i--;
 }


phoneAddress newPhone(char *email)
{
  phoneAddress phone;
  strcpy(phone.email, email);
  return phone;
}


void appendData(char *fileName, phoneAddress phone)
{
  FILE *f = fopen(fileName, "a");

  fprintf(f, "%s\t%s\t%s\n", phone.name, phone.tel, phone.email);
  printPhone(phone);
  fclose(f);
}




void phoneAssign(phoneAddress *phone1, phoneAddress *phone2)
{
  strcpy(phone1->name, phone2->name);
  strcpy(phone1->tel, phone2->tel);
  strcpy(phone1->email, phone2->email);
}


phoneAddress nullPhone(){
  phoneAddress newOne;
  strcpy(newOne.name, "");
  strcpy(newOne.tel, "");
  strcpy(newOne.email, "");
  return newOne;
}

phoneAddress makePhone(char *email)
{
  phoneAddress phone;
  strcpy(phone.name, "");
  strcpy(phone.email, email);
  strcpy(phone.tel, "");
  return phone;
}

void appendDataConst(phoneAddress phone)
{
  appendData(FILE_OUT, phone);
}

void clearFile(char *fileName)
{
  FILE *f = fopen(fileName, "w");
  fclose(f);
}
