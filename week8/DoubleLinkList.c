#ifndef _DOUBLELINKLIST_H_
#define _DOUBLELINKLIST_H_

#include <stdio.h>
#include <stdlib.h>
#include "dataType.h"
#include "DoubleLinkList.h"





NodeDList *makeNode(ElType data)
{

  NodeDList *temp = malloc(sizeof(NodeDList));

  temp->next = NULL;
  temp->prev = NULL;
  temp->data = data;
  return temp;
}



DoubleLinkList *makeLinkList()
{
  DoubleLinkList *new = malloc(sizeof(DoubleLinkList));
  new->root = NULL;
  new->current = NULL;
  return new;
}


//insert
void insertAtHead(ElType data, DoubleLinkList *list)
{

  if(list->root == NULL)
    {

      list->root = makeNode(data);
      list->current = list->root;
      return;
    };


  NodeDList *temp = makeNode(data);
  temp->next = list->root;
  (list->root)->prev = temp;
  temp->prev = NULL;
 list->root = temp;
}



void insertAtCurrent(ElType data, DoubleLinkList *list)
{

  if(list->root == NULL)
    {
      insertAtHead(data, list);
      return;
    };


  NodeDList *tempNext = (list->current)->next;
  NodeDList *temp = makeNode(data);
  if(tempNext == NULL)
    {
      (list->current)->next = temp;
      temp->prev = list->current;
      return;
    };

  temp->next = tempNext;
  tempNext->prev = temp;
  (list->current)->next = temp;
  temp->prev = (list->current);
  (list->current) = temp;
}



void insertBeforeCurrent(ElType data, DoubleLinkList *list)
{
  if(list->root== NULL)
    {
      insertAtHead(data,list);
      return;
    };

  NodeDList *temp = makeNode(data);
  if((list->current)->prev == NULL)
    {
      (list->current)->prev = temp;
      temp->next = list->current;
      return;
    };


  temp->prev = (list->current)->prev;
  (list->current)->prev = temp;
  temp->next = (list->current)->next;
  (temp->prev)->next = temp;
}




void insertAfterPosition(ElType data, int index, DoubleLinkList *list)
{
  NodeDList *temp = searchByIndex(index, list);

  if(temp == NULL)
    {
      printf("Index not available\n");
      return;
    };

  NodeDList *temp2 = makeNode(data);
  if(temp->next == NULL)
    {
      temp->next = temp2;
      temp2->prev = temp;
      return;
    };
  

  temp2->next  = temp->next;
  temp->next = temp2;
  temp2->prev =temp;
  (temp2->next)->prev  = temp2;
}


void insertBeforePosition(ElType data, int index, DoubleLinkList *list)
{
  NodeDList *temp = searchByIndex(index, list);
  if(temp == NULL)
    {
      printf("Index not available\n");
      return ;
    };

  NodeDList *newNode = makeNode(data);
  if(temp->prev == NULL)
    {
      temp->prev = newNode;
      newNode->next = temp;
      return;
    };

  newNode->prev = temp->prev;
  temp->prev =newNode;
  newNode->next = temp;
  (newNode->prev)->next = newNode;
}

//delete
void deleteAtPosition(int index, DoubleLinkList *list)
{
  NodeDList *temp = searchByIndex(index, list);
  if(temp == NULL)
    {
      printf("position not available\n");
      return;
    };


  (temp->prev)->next = temp->next;
  (temp->next)->prev = temp->prev;
  //ElType a = temp->data;
  free(temp);
  //  return a;
}

void deleteCurrent(DoubleLinkList *list)
{
  if(list->current == NULL)
    {
      printf("Can not delete current\n");
      return;
    };
  NodeDList *temp = list->current;

  if((list->current)->next == NULL)
    {
      if((list->current)->prev == NULL)
	{
	  free(list->current);
	  return;

	};

      list->current = (list->current)->prev;
      free(temp);
      return;

    };

  (temp->prev)->next = temp->next;
  (temp->next)->prev = temp->prev;
  list->current = temp->next;
  //ElType a = temp->data;
  free(temp);
  //return a;
  
}

void deleteLastPosition(DoubleLinkList *list)
{

  NodeDList *temp = searchLastPosition(list);

  if(temp == NULL)
    {
      printf("List not available\n");
      return;
    };

  if(temp->next == NULL)
    {
      free(temp);
    };

  (temp->prev)->next = temp->next;
  (temp->next)->prev = temp->prev;
  //ElType a= temp->data;
  free(temp);
  //return a;
}

void deleteNode(DoubleLinkList *list, NodeDList *temp)
{
  if(temp == NULL)
    {
      return;
    };

  if(list->root == NULL)
    {
      return;
    };

  (temp->prev)->next = temp->next;
  (temp->next)->prev = temp->prev;
  //ElType a = temp->data;
  free(temp);
  //return a;
}


void freeAll(DoubleLinkList *list)
{
  if(list->root == NULL)
    {
      printf("Empty list\n");
      return;
    };
  NodeDList *trial;
  NodeDList *temp = list->root;
  while(temp != NULL)
    {
      trial = temp->next;
      temp= temp->next;
      free(trial);
    };

}

//search



NodeDList *searchLastPosition(DoubleLinkList *list)
{

  if(list->root == NULL)
    {
      printf("Empty list\n");
      return NULL;
    };

  NodeDList *temp = list->root;
  while(temp->next != NULL)
    {
      temp = temp->next;
    };
  return temp;
}

  

NodeDList *searchByData(ElType data, DoubleLinkList *list, int (*compare)(ElType, ElType))
{

  if(list->root == NULL)
    {
      printf("Empty list\n");
      return NULL;
    };

  NodeDList *temp = list->root;

  while(temp != NULL)
    {

      if( (int)compare(temp->data, data) == 1)
	{
	  return temp;
	};

      temp = temp->next;
    };

  return NULL;
}


NodeDList *searchByIndex(int index, DoubleLinkList *list)
{
  if(list->root == NULL)
    {
      printf("Empty list \n");
      return NULL;
    };

  NodeDList *temp = list->root;
  int i = 0;
  while(temp != NULL)
    {
      if(i == index) return temp;
      i++;
      temp = temp->next;
    };

  return NULL;

}




//file interact
void saveIntoFile(char *fileName, DoubleLinkList *list)
{
  if(list->root == NULL)
    {
      printf("list not available\n");
      return;
    };


  FILE *f = fopen(fileName, "wb");

  NodeDList *temp = list->root;
  while(temp != NULL)
    {
      fwrite(&(temp->data), sizeof(ElType), 1, f);
      temp = temp->next;
    };

  fclose(f);
  return;
}


void readFromFile(char *fileName, DoubleLinkList *list)
{
  FILE *f = fopen(fileName,"rb");

  if(f == NULL)
    {
      printf("File not available\n");
      return;
    };

  ElType data;
  while(fread(&data, 1, sizeof(ElType), f) != 0)
    {

      insertAtCurrent(data, list);
    };

  fclose(f);
}
//Interface

//void printNode(NodeDList *new)/
//{

// printData(new->data);
//};


void display(DoubleLinkList *list, void (*act)(NodeDList*))
{
  if(list->root == NULL)
    {
      printf("Empty list \n");
      return;
    };


  NodeDList *temp = list->root;

  while(temp != NULL)
    {

      act(temp);
      printf("---------------\n");
      temp = temp->next;
    };

}

//Move to


void moveToFront(DoubleLinkList *list, NodeDList *temp)
{
  if(list->root == NULL)
    {
      printf("Emtpy list \n");
      return;
    };

  NodeDList *temp2 = temp;
  temp->next = list->root;
  list->root = temp2;

  deleteNode(list, temp2);
}
  

void moveToPrev(NodeDList *temp)
{
  if(temp->prev == NULL)
    {
      return;
    };

  ElType data = temp->data;
  temp->data = (temp->prev)->data;
  (temp->prev)->data = data;
}

#endif
