#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dataType.h"
#include "DoubleLinkList.h"
#include "phoneDeal.h"


void getData(DoubleLinkList *list, char *fileName)
{
  FILE* f = fopen(fileName, "r");
  if(f == NULL)
    {
      printf("Can not open file \n");
      return;
    };
  char *buffer = (char*)malloc(sizeof(char) * 100);

  char *token = (char*)malloc(sizeof(char) * 100);

  char *s = "\n\t";

  phoneAddress phone;

  while(fgets(buffer, 100, f) != NULL)
    {
      token = strtok(buffer, s);

      strcpy(phone.name, token);

      token = strtok(NULL, s);

      strcpy(phone.tel, token);

      token = strtok(NULL, s);

      strcpy(phone.email, token);

      insertAtCurrent(phone, list);
    };

  fclose(f);
}


int emailCompare(phoneAddress a, phoneAddress b)
{
  return (int)strcmp(a.email, b.email);
}



phoneAddress manualData()
{
  phoneAddress a;
  printf("Input name : ");
  gets(a.name);
  printf("Input telephone number: ");
  gets(a.tel);
  printf("Input email: ");
  gets(a.email);
  return a;
}


void printPhone(NodeDList *a)
{
  printf("%s\t\t%s\t\t%s\n", (a->data).name, (a->data).tel, (a->data).email);
}
