#include "dataType.h"

NodeDList *makeNode(ElType data);

DoubleLinkList *makeLinkList();

//insert
void insertAtHead(ElType data, DoubleLinkList *list);

void insertAtCurrent(ElType data, DoubleLinkList *list);

void insertBeforeCurrent(ElType data, DoubleLinkList *list);

void insertAfterPosition(ElType data, int index, DoubleLinkList *list);

void insertBeforePosition(ElType data, int index, DoubleLinkList *list);

//delete
void deleteAtPosition(int index, DoubleLinkList *list);

void deleteCurrent(DoubleLinkList *list);

void deleteLastPosition(DoubleLinkList *list);

void freeAll(DoubleLinkList *list);

void deleteNode(DoubleLinkList *list, NodeDList *temp);

//file interact
void saveIntoFile(char *fileName, DoubleLinkList *list);

void readFromFile(char *fileName, DoubleLinkList *list);

//searchNode
NodeDList *searchByData(ElType data, DoubleLinkList *list, int (*compare)(ElType, ElType));

NodeDList *searchByIndex(int index, DoubleLinkList *list);

NodeDList *searchLastPosition(DoubleLinkList *list);

//Interface
//void printNode(NodeDList *new);

void display(DoubleLinkList *list, void (*act)(NodeDList*));


//Move to
void moveToFront(DoubleLinkList *list, NodeDList *temp);

void moveToPrev(NodeDList *temp);

