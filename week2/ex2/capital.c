#include <stdio.h>
#include <stdlib.h>

void capital_change(FILE *fin, FILE *fout);
enum {
    SUCCESS, FAIL};


int main()
{
    File *f1, *f2;
    printf("Input the file name \n");
    char *file_name = (char *)malloc(sizeof(char) *100);
    gets(file_name);
    int result;
    if( (f1 = fopen(file_name))== NULL)
	{
	    printf("Unavailable \n");
	    result = FAIL;
	}
    else
	{
	    f2 = fopen("output.txt", "w");
	    capital_change(f1, f2);
	    result= SUCCESS;
	};
    return result;
}

void capital_change(FILE *f1, FILE *f2)
{
    int c;
    while( (c = fgetc(f1)) != EOF)
	{
	    if( c>=65 && c<=90) c+=32;
	    fputc(c, f2);
	    putchar(c);
	};
}
