#include <stdio.h>
#include <stdlib.h>

void capital_change(FILE *f1, FILE *f2);
enum {
    SUCCESS, FAIL};


int main()
{
    FILE *f1, *f2;
    printf("Input the file name \n");
    char *file_name = (char *)malloc(sizeof(char) *100);
    gets(file_name);
    int result;
    if( (f1 = fopen(file_name, "r"))== NULL)
	{
	    printf("Unavailable \n");
	    result = FAIL;
	}
    else
	{
	    f2 = fopen("output.txt", "w");
	    capital_change(f1, f2);
	    result= SUCCESS;
	};
    return result;
}

void capital_change(FILE *f1, FILE *f2)
{
    int c;
    while( (c = fgetc(f1)) != EOF)
	{
	    if( c>=65 && c<=90) c+=32;
	    //else if( c>=97 && c<=122)c-=32;
	    fputc(c, f2);
	    putchar(c);
	};
}

void code(FILE *f1, FILE *f2, int n)
{
    int c;
    while( c = fgetc(f1) != EOF)
	{
	    if(c> (90-n))
		{
		    c = 65 + n - (90 -c);
		}
	    else if( c > (122-n))
		{
		    c = 97 + n - (122 - c);
		};
	    fputc(c, f2);
	};
}

void over_write(FILE *f1, FILE *f2 , char *file_name1, char *file_name2)
{
    f1 = fopen(file_name1, "w");
    f2 = fopen(file_name2, "r");
    int d;
    //char *buff = (char *)malloc(sizeof(buff) *100);
    // while(fgets(buff, 100, f2) != NULL)
    while((d = fgetc(f2)) != EOF)
	{
	    fputc(d, f1);
	    putchar(d);
	};
}
		    
		    
