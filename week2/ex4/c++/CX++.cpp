#include <iostream>
#include <string>
#include <fstream>

using namespace std;

enum
    {
	SUCCESS, FAIL
    };


class File
{
public:
    void my_cat(const char * name1, const char * name2)
    {
	//cout<<name1<<endl;
	//char *file_name1 = convert(name1);
	//cout<<file_name1<<endl;
	//char *file_name2 = convert(name2);
	//cout<<file_name2<<endl;
	
	ifstream fin(name1);//fin(file_name1);
	if( !fin.is_open())
	  {
		cout<<"Can not open file \n";
		return;
	  };
	ofstream fout(name2);//fout(file_name2);
	char c ;
	while( fin.get(c) )
	    {
		fout.put(c);
		cout.put(c);
	    };
	fin.close();
	fout.close();
	//delete file_name1;
	//delete file_name2;
    }

     void capital_off(const char* file_name1, const char * file_name2)
    {
	char c;
	ifstream fin(file_name1);
	if( !fin.is_open())
	 {
		cout<<"File unavailable \n";
		return;
	   };
	ofstream fout(file_name2);
	while( fin.get(c) )
	    {
		if( c>=65 && c<=90)
		    {
			c+=32;
		    };
		fout.put(c);
		cout.put(c);
	    };
	fin.close();
	fout.close();
    }

    void code(const char *file_name1 , const char *file_name2 , int n)
    {
	ifstream fin(file_name1);
       if( !fin.is_open())
	  {
		cout<<"File unavailable \n";
		return;
	  };
	ofstream fout(file_name2);
	char c;
	while( fin.get(c) )
	    {
		if( c>= 65 && c<=90)
		    {
			if( 90 - c < n)
			    {
				c = 64 + n - (90 - c);
			    }
			else c+=n;
		    }
		else if( c>= 97 && c<=122)
		    {
			if( 122 -c < n)
			    {
				c = 97 + n - (122 - c);
			    }
			else c+=n;
		    }
		else if ( c == 32 || c== 10)
		    {
		    }
		else
		    {
			c = c+n;
		    };
		fout.put(c);
		//cout.put(c);
	    };
	fin.close();
	fout.close();
	my_cat(file_name2, file_name1);
	//	mycat(name2, name1);
    }

    void decode (const char *file_name1, const char *file_name2 , int n)
    {
	ifstream fin;
	ofstream fout;
	char c;
	fin.open(file_name1);
	if(!fin.is_open())
	  {
		cout<<"File unavailable \n";
		return;
	 };
	fout.open(file_name2);
	while( fin.get(c) )
	    {
		if( c >= 65 && c<= 90)
		    {
			if( c - 65 < n)
			    {
				c = 90 - ( n - (c-64));
			    }
			else
			    c-=n;
		    }
		else if( c>= 97 && c<= 122)
		    {
			if ( c- 96 <= n)
			    {
				c = 122 - (n - ( c-97));
			    }
			else
			    c-=n;
		    }
		else if( c == 32 || c == 10)
		    {
		    }
		else
		    {
			c-= n;
		    };
		fout.put(c);
		//cout.put(c);
	    };
	fin.close();
	fout.close();
	//mycat(name2, name1);
	my_cat(file_name2, file_name1);
    }

    void char_count(const char *file_name1, const char *file_name2)
    {
	int *count = new int[500];
	char i;
	char c;
	ifstream fin(file_name1);
	ofstream fout(file_name2);
	if(!fin.is_open())
	    {
		cout<<"File unavailable \n";
		return;
	    };
	for(i = 0; i<=122; i++)
	    {
		count[i] = 0;
	    };
	while( fin.get(c))
	    {
		count[c]++;
	    };
	for( i = 0 ;i <=122; i++)
	    {
		if( i != 10 && i !=32 && count[i] != 0)
		    {
			cout<< i << ": " <<count[i]<<endl;
			fout<< i << ": " <<count[i]<<endl;
		    };
	    };
	//cout<<"Non sense \n";
	//cout<<"1";
	//	cout<<"ping1"<<endl;
	//	cout<<"ping2"<<endl;
	//	cout<<"ping3"<<endl;
	//delete[] count;
	//	cout<<"2";
	//	cout<<"ping4"<<endl;
	fin.close();
	fout.close();
	//return;
    }

    void uncap_char_count(const char *file_name1, const char *file_name2)
    {
	capital_off(file_name1, file_name2);
	my_cat(file_name2, "temp.txt");
	char_count("temp.txt", file_name2);
    }
    
};
   
int main()
{
    int k;
    char *name1 = new char[40];
    char *name2 = new char[40];
    cout<<"Input file name 1 \n";
    cin.getline(name1, 40);
    File ob;
    
    do
	{
    cout<<"MENU \n";
    cout<<"1. capital \n";
    cout<<"2. my cat \n";
    cout<<"3. code \n";
    cout<<"4. decode \n";
    cout<<"5. Char count \n";
    cout<<"6. Uncap char cout\n";
    cout<<"7. Exit \n";
    cout<<"Your choice: ";
    cin>>k;
    //cin.ignore();
    if( k == 1)
	{
	    ob.capital_off(name1, "output.txt");
	    cout<<endl;
	};
    if(k ==2)
	{
	    ob.my_cat(name1, "output.txt");
	};
    if(k ==3)
	{
	    int n;
	    cout<<"Input the distance \n";
	    cin>>n;
	    //cin.ignore();
	    ob.code(name1, "output.txt", n);
	};
    if(k == 4)
	{
	    int n;
	    cout<<"Input the distance \n";
	    cin>>n;
	    //cin.ignore();
	    ob.decode(name1, "output.txt", n);
	};
    if(k == 5)
	{
	    ob.char_count(name1, "sourcestat.txt");
	};
    if(k == 6)
	{
	    ob.uncap_char_count(name1, "sourcestat2.txt");
	};

    // cout.flush();
	}while( k != 7);
    return 0;
}
	    
