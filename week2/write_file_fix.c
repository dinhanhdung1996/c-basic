#include <stdio.h>
#include <stdlib.h>

void charReadWrite();

int main()
{
    FILE *fptr1, *fptr2;
    char filename1[] = "lab1.txt";
    char filename2[] = "lab1w.txt";

    if((fptr1 = fopen(filename1, "r")) == NULL)
	{
	    printf("Can't open %s", filename1);
	}
    else if( (fptr2 = fopen(filename2, "w")) == NULL)
	{
	   printf("Can't open %s", filename2);
	}
    else
	{
	    charReadWrite(fptr1, fptr2);
	    fclose(fptr1);
	    fclose(fptr2);
	};
    return 0;
}

void charReadWrite(FILE *f1, FILE *f2)
{
    int c;
    while( (c = fgetc(f1)) != EOF)
	{
	    fputc(c, f2);
	    printf("%d", c);
	    //putchar(c);
	};
}
