#include <stdio.h>
#include <stdlib.h>
#include "stack.h"

int main()
{
    int k;
    Stack *stack1 = makeStack();
    Stack *stack2 = makeStack();
    Stack *stack3 = makeStack();
    int number;
    int method;
    do
    {
        printf("Menu \n");
        printf("1. Add integer into stack\n");
        printf("2. View top\n");
        printf("3. Take out one element\n");
        printf("4. Undo \n");

        printf("5. exit\n");


        printf("Your choice: ");
        scanf("%d", &k);
        while(getchar() != '\n');


        switch(k)
        {
        case 1:
            printf("Input an integer: ");

            scanf("%d", &number);
            while(getchar() != '\n');

            push(number, stack1);
            push(1, stack3);
            break;
        case 2:

            number = viewTop(stack1);
            printf("%d\n", number);
            break;
        case 3:
            number = pop(stack1);
            push(0, stack3);
            push(number, stack2);
            break;
        case 4:
            method = pop(stack3);
            if(method == 1)
            {
                pop(stack1);
                //break;
            };
            if(method == 0)
            {
                int number = pop(stack2);
                push(number, stack1);
            };

            break;
        default:
            break;
        };

    }while(k != 5);

    return 0;
}

