#include <stdio.h>
#include <stdlib.h>
#include "stack.h"



Node *makeNode(ElType data)
{
    Node *temp = malloc(sizeof(Node));
    temp->data = data;
    temp->next = NULL;
    return temp;
}


Stack *makeStack()
{
    Stack *stack = malloc(sizeof(stack));

    stack->top = NULL;
    return stack;
}


void push(ElType data, Stack *stack)
{
    if(stack->top == NULL)
    {
        stack->top = makeNode(data);
        return;
    };


    Node *temp = makeNode(data);
    temp->next = stack->top;
    stack->top = temp;
}


ElType pop(Stack *stack)
{
    if(stack->top == NULL)
    {
        printf("Stack unavailable\n");
        return -1;
    };

    Node *temp = stack->top;
    ElType a = temp->data;
    stack->top = temp->next;
    free(temp);
    return a;
}


ElType viewTop(Stack *stack)
{
    if(stack->top == NULL)
    {
        printf("Empty stack\n");
        return -1;
    };
    return (stack->top)->data;
}

