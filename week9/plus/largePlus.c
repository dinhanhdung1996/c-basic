#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef int ElType;


typedef struct Node{
  ElType data;
  struct Node *next;
}Node;


typedef struct Stack{
  Node *top;
}Stack;



Node *makeNode(ElType data)
{
  Node * newNode = malloc(sizeof(Node));
  newNode->next = NULL;
  newNode->data = data;
  return newNode;
}


Stack *makeStack()
{
  Stack *newStack = malloc(sizeof(Stack));
  newStack->top = NULL;
  return newStack;
}


void push(ElType data, Stack *stack)
{

  if(stack->top == NULL)
    {
      stack->top = makeNode(data);
      return;
    };

  Node *temp = makeNode(data);
  temp->next = stack->top;
  stack->top = temp;
}

ElType pop(Stack *stack)
{
  if(stack->top == NULL)
    {
      printf("Unavailable stack\n");
      return 0;
    };


  Node *newNode = stack->top;
  stack->top = newNode->next;
  ElType data = newNode->data;
  free(newNode);
  return data;
}



int main()
{
  char *firstNum = (char *) malloc(sizeof(Stack) * 100);
  char *secondNum = (char *)malloc(sizeof(Stack) * 100);
  char *thirdNum = (char *)malloc(sizeof(Stack) * 100);

  Stack *stack1 = makeStack();
  Stack *stack2 = makeStack();
  Stack *stack3 = makeStack();
  //Stack *remain = makeStack();
  printf("Input the first number: ");
  gets(firstNum);
  printf("Input the second number: ");
  gets(secondNum);
  
  int i=0;
  while( firstNum[i] != '\0')
    {
      push(firstNum[i]- '0', stack1);
      i++;
    };
  i = 0;

  while(secondNum[i] != '\0')
    {
      push(secondNum[i] - '0', stack2);
      i++;
    };

  int a;
  int b;
  int sum;
  int remain = 0;
  while(1)
    {
      if(stack1->top == NULL)
	{
	  while(stack2->top != NULL)
	    {
	      a = pop(stack2);
	      if(remain == 1)
		{
		  a = a+ 1;
		  remain = 0;
		};
	      if(a > 9)
		{
		  a = a - 10;
		  remain  =1;
		};
	      
	      push(a, stack3);
	    };
	  break;
	  
	};

      if(stack2->top == NULL)
	{
	  while(stack1->top != NULL)
	    {
	      a = pop(stack1);
		{
		  if(remain==1)
		    {
		      a = a+1;
		      remain = 0;
		    };
		  if(a>9)
		    {
		      a = a - 10;
		      remain = 1;
		    };
		  push(a, stack3);
		};
	    };
	  break;
	};
      
	      
      a = pop(stack1);
      b = pop(stack2);

      sum = a + b;
      if(remain == 1)
	{
	  sum = sum + 1;
	  remain = 0;
	};
      if(sum > 9)
	{
	  sum = sum- 10;
	  remain = 1;
	};
      

      push(sum, stack3);
      
    }

  while(stack3->top != NULL)
    {
      a = pop(stack3);
      printf("%d", a);
    };

  printf("\n");
  return 0;
}
      
