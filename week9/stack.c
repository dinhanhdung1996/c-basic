#include <stdio.h>
#include <stdlib.h>



typedef int Eltype;
typedef struct node{
  Eltype data;
  struct node *next;
}node;

//typedef struct Stack{
// node *top;
  //}Stack

node *top;


node *makeNode(Eltype data)
{
  node *newNode = malloc(sizeof(node));
  newNode->next = NULL;
  newNode->data = data;
  return newNode;
}



void push(Eltype data)
{
  node *newNode= makeNode(data);
  if(top == NULL)
    {
      top = newNode;
      return;
    };
  newNode->next = top;
  top = newNode;
}


Eltype pop()
{
  if(top == NULL)
    {
      printf("Stack empty\n");
      return 0;
    };


  node *temp = top;
  top = temp->next;
  Eltype a = temp->data;
  free(temp);
  return a;
}





int main()
{

  printf("Input the decimal : ");
  int decimal;
  scanf("%d", &decimal);

  int i = 0;
  int take = decimal;
  int remain;
  while(take >= 1)
    {
      remain = take % 2;
      // printf("%d - ", remain);
      push(remain);
      take = take/2;
      //printf("%d\n",take);
    };

  while(top != NULL)
    {
      int a = pop();
      printf("%d", a);
    };
  printf("\n");

  return 0;
}
  
