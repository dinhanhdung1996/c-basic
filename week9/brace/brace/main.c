#include <stdio.h>
#include <stdlib.h>
#include "stack.h"


Stack *stack1;
Stack *stack2;

void readIn(char *fileName)
{
  FILE *f = fopen(fileName, "r");
  if(f == NULL)
    {
      printf("File not available\n");
      return ;
    };

  int c;
  while( (c= fgetc(f)) != EOF)
    {
      if(c == '{' || c == '(' || c == '[')
	{
	  push(c, stack1);
	   printf("%c\n", c);
	};
      if(c == '}' || c == ')' || c == ']')
	{
	  push(c, stack2);
	   printf("%c\n", c);
	};

    };

  fclose(f);
}

int try(int a, int b)
{

  if(a == 40 && b==41) return 1;
  if(a == 91 && b==93) return 1;
  if(a== 123 && b==125) return 1;

  return 0;
}


int main()
{
  printf("Input file name: ");
  char *fileName = (char *)malloc(sizeof(char) * 100);

  gets(fileName);

  stack1 = makeStack();
  stack2 = makeStack();
  readIn(fileName);
  int a;
  int b;

  
  while( 1) {

    if(stack1->top == NULL)
      {
	if(stack2->top == NULL)
	  {
	    printf("Match\n");
	    break;
	  }
	else
	  {
	    printf("fault: %c\n", (stack2->top)->data);
	    printf(" Not Match\n");
	    break;
	  };

      }
    else
      {
	if(stack2->top == NULL)
	  {
	    printf("fault: %c\n", (stack1->top)->data);
	    printf("Not match\n");
	    break;
	  }
	//	printf("1\n");
      };


    a = pop(stack1);
    b = pop(stack2);
    if(try(a, b) == 1)continue;
    else
      {
	printf("fault: %c", a);
	printf("Not match\n");
	break;
      };
  };

  return 0;
}
    
