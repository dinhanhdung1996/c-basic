#ifndef _DATATYPE_H
#define _DATATYPE_H




typedef struct phoneAddress{
  char tel[40];
  char name[40];
  char email[40];
}phoneAddress;

typedef struct Student{
  char username[30];
  char password[30];
  float score;}Student;

typedef Student ElType;

typedef struct Node{
  ElType key;
  struct Node *left, *right;
}Node;


typedef Node *Tree;

typedef struct NodeList{
    ElType data;
    struct NodeList *next;
}NodeList;


typedef struct Stack{
	NodeList *top;
}Stack;

typedef struct Queue{
  NodeList *top;
}Queue;

#endif
