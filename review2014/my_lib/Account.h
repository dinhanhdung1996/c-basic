#include "dataType.h"


void userMenu();

void adminMenu();

void changePassword(Tree tree);

void saveToFile(Tree tree);

void printData(Tree tree);

void printDataAdmin(Tree tree);

int compareName(Student a, Student b);

void getData(Tree *tree);

Student makeStudent(char *username);

void clearFile();
