#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dataType.h"
#include "Account.h"
#include "BinarySearchTree.h"



int main()
{

  Tree tree = makeNullTree();

  int choice;
  getData(&tree);

  do
    {
    menu:
      printf("Menu\n");
      printf("1. Log in\n");
      printf("2. Exit\n");
      printf("choice: ");
      scanf("%d", &choice);

      while(getchar() != '\n');


      if(choice == 1)
	{
	  char *username =(char*)malloc(sizeof(char) * 100);
	  char *password =(char*)malloc(sizeof(char) * 100);
	  printf("Input username: ");
	  gets(username);
	  printf("Input password: ");
	  gets(password);
	  

	  Tree new = searchNode(tree, makeStudent(username), &compareName);
	  if(new == NULL){
	    printf("Wrong username\n");
	    goto menu;
	  }
	  else{
	    if(strcmp((new->key).password, password) != 0)
	      {
		printf("Wrong password\n");
		goto menu;
	      };
		
	  };

	  if(strcmp((new->key).username, "Admin") != 0)
	    {
	      int userChoice;
	      do
		{
		  userMenu();
		  printf("Your choice : ");
		  scanf("%d", &userChoice);
		  while(getchar() != '\n');

		  
		  if(userChoice == 1)
		    {
		    printf("Data of user %s :", (new->key).username);
		    printData(new);
		    };

		  if(userChoice == 2)
		    {
		      changePassword(new);
		    };

		  if(userChoice == 3)
		    {
		      clearFile();
		      preOrderTraversal(tree, &saveToFile);
		    };
		      
		} while( userChoice != 3);
	    }
	  else
	    {
	      
	      int adminChoice;
	      do{
	      admin:
	      adminMenu();
	      printf("Your choice: ");
	      scanf("%d", &adminChoice);
	      while(getchar() != '\n');
	      if(adminChoice == 1)
		{
		  Student student;
		  printf("Input username: ");
		  gets(student.username);
		  printf("Input password: ");
		  gets(student.password);
		  printf("Input score: ");
		  scanf("%f", &student.score);
		  while(getchar() != '\n');
		  insertNode(&tree, student, &compareName);
		};


	      if(adminChoice == 2)
		{
		  preOrderTraversal(tree, &printDataAdmin);
		};

	      if(adminChoice == 3)
		{
		  printf("Input the name of user: ");
		  char *user = (char*)malloc(sizeof(char) * 100);
		  gets(user);
		  Student student = makeStudent(user);
		  Tree getStudent = searchNode(tree, student, &compareName);
		  if(getStudent == NULL){
		    printf("not available\n");
		    goto admin;
		  }
		  else
		    {
		      printData(getStudent);
		      if(strcmp((getStudent->key).username, "Admin") == 0){
			printf("Can not delete Admin user\n");
			goto admin;
		      }
		      else
			{
			  deleteNode(getStudent->key, &tree, &compareName);
			};
		    };
		};

	      if(adminChoice == 4)
		{
		  clearFile();
		  preOrderTraversal(tree, &saveToFile);
		};
	      }while( adminChoice != 4);
	    }
	};

      if(choice == 2)
	printf("Good bye! see you later\n");
    }while( choice != 2);
  return 0;
}
	      

