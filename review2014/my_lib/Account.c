#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dataType.h"
#include "BinarySearchTree.h"
#include "Account.h"
#define FILE_NAME "sinhvien.txt"


void userMenu()
{
  printf("MENU \n");
  printf("1. Show score of student\n");
  printf("2. Change password \n");
  printf("3. Save to file and exit\n");
}


void adminMenu()
{
  printf("MENU\n");
  printf("1. Add a student\n");
  printf("2. print out data of student \n");
  printf("3. Delete a student\n");
  printf("4. Save data to file\n");
}


void changePassword(Tree tree)
{
  char *buffer1 = (char* )malloc(sizeof(char) * 100);
  char *buffer2 = (char *)malloc(sizeof(char) * 100);

  printf("Input password: ");
  gets(buffer1);
  printf("Repeat password field: ");
  gets(buffer2);
  if(strcmp(buffer1, buffer2) == 0) strcpy((tree->key).password, buffer1);
  else printf("password is not matched\n");
}

void clearFile()
{
  FILE *f = fopen(FILE_NAME, "w");
  fclose(f);
}


void saveToFile(Tree tree)
{
  FILE *f = fopen(FILE_NAME, "a");
  fprintf(f, "%s\t\t%s\t\t%.2f\n", (tree->key).username, (tree->key).password, (tree->key).score);
  fclose(f);
}


void printData(Tree tree)
{
  printf("%s\t%s\t%f\n", (tree->key).username, (tree->key).password, (tree->key).score);
}

void printDataAdmin(Tree tree)
{
  if(strcmp((tree->key).username, "Admin") == 0)return;
  else printData(tree);
}

int compareName(Student a, Student b)
{
  return (int)strcmp(a.username, b.username);
}


void getData(Tree *tree)
{
  FILE *f = fopen(FILE_NAME, "r");

  char *buffer= (char*)malloc(sizeof(char) * 100);

  char *token = (char*)malloc(sizeof(char) * 100);
  char *s = "\t\n";

  Student student;

  while(fgets(buffer, 100, f) != NULL)
    {
      token = strtok(buffer, s);
      strcpy(student.username, token);
      token = strtok(NULL, s);
      strcpy(student.password, token);
      token = strtok(NULL, s);
      student.score = atoi(token);
      insertNode(tree, student, &compareName);
    };
}

Student makeStudent(char *username)
{
  Student student;
  strcpy(student.username, username);
  strcpy(student.password, "");
  student.score = 0;
  return student;
}
