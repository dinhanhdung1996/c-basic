#include "dataType.h"



Queue *makeQueue();

NodeList *makeNodeQueue(ElType data);

void enqueue(Queue *queue, ElType data);

ElType dequeue(Queue *queue, ElType (*nullElType)());

int isEmpty(Queue *queue);

ElType viewTopQueue(Queue *queue);

  
  
