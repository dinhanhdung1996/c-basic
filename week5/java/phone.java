import java.util.*;
import java.io.*;


class phone implements Serializable
{

    private String model;
    private String ROM;
    private String screen;
    private double price;

    public void setModel(String model)
    {
	this.model = model;
    }

    public void setROM(String ROM)
    {
	this.ROM = ROM;
    }


    public void setScreen(String screen)
    {
	this.screen = screen;
    }

    public void setPrice(double price)
    {
	this.price = price;
    }

    public String getModel()
    {
	return model;
    }

    public String getROM()
    {
	return ROM;
    }

    public String getScreen()
    {
	return screen;
    }

    public double getPrice()
    {
	return price;
    }

    


    public String toString()
    {
	return model + "\t\t" + ROM + "\t" + screen + "\t" + price;
    }


}
