#include <stdio.h>
#include <stdlib.h>
#include "dataType.h"
#include "stack.h"



NodeList *makeNodeStack(ElType data)
{
    NodeList *temp = malloc(sizeof(NodeList));
    temp->data = data;
    temp->next = NULL;
    return temp;
}


Stack *makeStack()
{
    Stack *stack = malloc(sizeof(stack));

    stack->top = NULL;
    return stack;
}


void push(ElType data, Stack *stack)
{
    if(stack->top == NULL)
    {
        stack->top = makeNodeStack(data);
        return;
    };


    NodeList *temp = makeNodeStack(data);
    temp->next = stack->top;
    stack->top = temp;
}


ElType pop(Stack *stack, ElType (*nullElType)())
{
    if(stack->top == NULL)
    {
        printf("Stack unavailable\n");
        return nullElType();
    };

    NodeList *temp = stack->top;
    ElType a = temp->data;
    stack->top = temp->next;
    free(temp);
    return a;
}


ElType viewTop(Stack *stack, ElType (*nullElType)())
{
    if(stack->top == NULL)
    {
        printf("Empty stack\n");
        return nullElType();
    };
    return (stack->top)->data;
}

