#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dataType.h"
#include "BinarySearchTree.h"
#include "phoneDeal.h"



int main()
{
  int choice;

  phoneAddress *phone = (phoneAddress *)malloc(sizeof(phoneAddress) * 100);

  Tree tree = NULL;
  int num = 0;
  do
    {
      printf("MENU \n");
      printf("1. Get in data \n");
      printf("2. Find a data \n");
      printf("3. Print all data \n");
      printf("4. Count number \n");
      printf("5. height of tree \n");

      printf("6. Exit \n");
      printf("Your chocie: ");
      scanf("%d", &choice);
      while(getchar() != '\n');


      if(choice == 1)
	{
	  num = getData("phoneBook.txt", phone);
   
	  int i;
	  for(i = 0 ; i < num ; i++)
	    {	  
	      insertNode(&tree, phone[i], &emailCompare);
	    };
	};

      if(choice == 2)
	{
	  printf("Input an email to search : ");
	  char *email = (char*)malloc(sizeof(char) * 100);

	  gets(email);

	  Node *result = searchNode(tree, newPhone(email), &emailCompare);
	  if(result == NULL) continue;
	  printf("%s\n", (result->key).email);
	  appendData("result.txt", result->key);
	};

      if(choice == 3)
	{
	  printf("post:\n");
	  postOrderTraversal(tree, &printListPhone);
	  printf("pre: \n");
	  preOrderTraversal(tree, &printListPhone);
	  printf("in: \n");
	  inOrderTraversal(tree, &printListPhone);
	};


      if(choice == 4)
	{
	  printf("number of node is %d\n", numberOfNode(tree));
	};


      if(choice == 5)
	{
	  printf("height tree is %d\n", heightTree(tree));
	};

    }
  while(choice != 6);

  return 0;
}
	  
