#include <stdio.h>
#include <stdlib.h>
#include "service.h"


Computer makeComputer(int memoryCapacity, int numberOfParallel)
{
  Computer new = malloc(sizeof(Computer));
  new->processes = makeQueue();
  new->memoryCapacity = memoryCapacity;
  new->numberOfParallel = numberOfParallel;
  return new;
}


Program makeProgram(int programID, int requiredMemory)
{
  Program program = malloc(sizeof(program));
  program->requriedMemory = requiredMemory;
  program->programID = programID;
  program->runningStatus = NONE;
}


int parallelProgram(Computer computer)
{
  if((computer->processes)->top == NULL)
    {
      return 0;
    };

  Node *temp = (computer->processes)->top;
  int count = 0;
  while(temp != NULL)
    {
      count++;
      temp = temp->next;
    };

  return count;
}


int usedMemory(Computer computer)
{
  if((computer->processes)->top == NULL)
    {
      return 0;
    };

  Node *temp = (computer->processes)->top;
  int sum = 0;
  while(temp != NULL)
    {
      sum = sum + (temp->data)->requiredMemory;
      temp = temp->next;
    };
  return sum;
}



int addProgram(Computer computer, Program program)
{
  int used = usedMemory(computer);
  if( ( used + (program->requiredMemory) ) > (computer->memoryCapacity) )
    {
      printf("Can not add this program - full of memory \n");
      return 0;
    }
  else
    {
      program->runningStatus = WAITING;
      enqueue(computer->processes, program);
      return 1;
    };
}


void checkProcesses(Computer computer)
{

  int i;
  Node *temp = (computer->processes)->top;
  for(i = 0; i < (computer->numberOfParallel); i++)
    {
      (temp->data)->runningStatus = RUNNING;
    };
}


Program killProgram(Computer computer)
{
  Program program = dequeue(computer->processes);
  checkProcesses(computer);
  return Program;
}


void printStatus(Computer computer)
{
  if( isEmpty(computer->processes) )
    {
      printf("Empty processes\n");
      return;
    };

  Node *temp = (computer->processes)->top;

  while(temp != NULL)
    {
      printf(" %d - %d - %s\n", (temp->data)->programID, (temp->data)->requiredMemory, getStatus((temp->data)->runningStatus));
    };
}


char *getStatus(enum RunningStatus runningStatus)
{
  switch(runningStatus) {
  case NONE: return "NONE";
  case RUNNING: return "RUNNING";
  case Waiting: return "WaitTing";
  default: return NULL;
  };
  return NULL;
}
