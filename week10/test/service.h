#include "queue.h"

enum RunningStatus{
  NONE,
  RUNNING,
  WAITING };


typedef struct Program
{
  int programID;
  int requiredMemory;
  enum RunningStatus runningStatus;
}*Program;

typedef struct Computer
{
  Queue *processes;
  int memoryCapacity;
  int numberOfParallel;
}*Computer;


int parallelProgram(Computer computer);

int usedMemory(Computer computer);

int addProgram(Computer computer, Program program);

void checkProcesses(Computer computer);

Program killProgram(Computer computer);

void printStatus(Computer computer);

char *getStatus(enum RunningStatus runningStatus);


