#include <stdio.h>
#include <stdlib.h>
//#include "queue.h"
#include "service.h"


int main()
{
  Computer computer = makeComputer(2048, 5);

  Program program;


  int choice;
  do
    {
      printf("MENU\n");
      printf("1. Add program\n");
      printf("2. Kill a program \n");
      printf("3. Show all processes\n");
      printf("Your choice: ");
      scanf("%d", &choice);
      while(getchar() != '\n');
      if(choice ==1)
	{
	  printf("Input ID of program: ");
	  int id;
	  scanf("%d", &id);
	  while(getchar() != '\n');

	  printf("Input required memory: ");
	  int mem;
	  scanf("%d", &mem);
	  while(getchar() != '\n');

	  program = makeProgram(id, mem);

	  int re = addProgram(computer, program);
	  if(re ==1 )
	    {
	      printf("SUCCESSFUL\n");
	      checkProcesses(computer);
	    }
	  else
	    printf("FAIL\n");
	};


      if(choice ==2)
	{
	  Program new = killProgram(computer);
	  if(new == NULL)
	    {
	      printf("FAIL ACTION\n");
	    }
	  else
	    printf("SUCCESSFUL\n");
	};

      if(choice ==3)
	{
	  printStatus(computer);
	};
      
    }while(choice !=4);

  return 0;
}
