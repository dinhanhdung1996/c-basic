#include "queue.h"




Computer makeComputer(int memoryCapacity, int numberOfParallel);

Program makeProgram(int programID, int requiredMemory);

int parallelProgram(Computer computer);

int usedMemory(Computer computer);

int addProgram(Computer computer, Program program);

void checkProcesses(Computer computer);

Program killProgram(Computer computer);

void printStatus(Computer computer);

char *getStatus(enum RunningStatus runningStatus);


  
