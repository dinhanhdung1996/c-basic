#include <stdio.h>
#include <stdlib.h>
#include "queue.h"


Queue *makeQueue()
{
  Queue *queue = malloc(sizeof(Queue));
  queue->top = NULL;
  return queue;
}


Node *makeNode(ElType data)
{
  Node *new = malloc(sizeof(Node));
  new->data = data;
  new->next = NULL;
  return new;
}


void enqueue(Queue *queue, ElType data)
{
  if(queue->top == NULL)
    {
      queue->top = makeNode(data);
      return;
    };

  Node *temp = queue->top;
  while(temp->next != NULL)
    {
      temp = temp->next;
    };
  temp->next = makeNode(data);
  
}


ElType dequeue(Queue *queue)
{
  if(queue->top == NULL)
    {
      printf("Empty list\n");
      return NULL;
    };

  Node *temp = queue->top;
  queue->top = temp->next;
  ElType a=  temp->data;
  free(temp);
  return a;
}


int isEmpty(Queue *queue)
{
  if(queue->top== NULL)
    {
      return 1;
    }
  else return 0;
}





  
  
