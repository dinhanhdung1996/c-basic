#include <stdio.h>
#include <stdlib.h>
#include "queue.h"


Queue *makeQueue()
{
  Queue *queue = malloc(sizeof(Queue));
  queue->top = NULL;
  return queue;
}


Node *makeNode(ElType data)
{
  Node *new = malloc(sizeof(Node));
  new->data = data;
  new->next = NULL;
  return new;
}


void enqueue(Queue *queue, ElType data)
{
  if(queue->top == NULL)
    {
      queue->top = makeNode(data);
      return;
    };

  Node *temp = queue->top;
  while(temp->next != NULL)
    {
      temp = temp->next;
    };
  temp->next = makeNode(data);
  
}


ElType dequeue(Queue *queue)
{
  if(queue->top == NULL)
    {
      printf("Empty list\n");
      return NULL;
    };

  Node *temp = queue->top;
  queue->top = temp->next;
  ElType a=  temp->data;
  free(temp);
  return a;
}


int isEmpty(Queue *queue)
{
  if(queue->top== NULL)
    {
      return 1;
    }
  else return 0;
}



Computer makeComputer(int memoryCapacity, int numberOfParallel)
{
  Computer new = malloc(sizeof(Computer));
  new->processes = makeQueue();
  new->memoryCapacity = memoryCapacity;
  new->numberOfParallel = numberOfParallel;
  return new;
}


Program makeProgram(int programID, int requiredMemory)
{
  Program program = malloc(sizeof(program));
  program->requiredMemory = requiredMemory;
  program->programID = programID;
  program->runningStatus = NONE;
}


int parallelProgram(Computer computer)
{
  if((computer->processes)->top == NULL)
    {
      return 0;
    };

  Node *temp = (computer->processes)->top;
  int count = 0;
  while(temp != NULL)
    {
      count++;
      temp = temp->next;
    };

  return count;
}


int usedMemory(Computer computer)
{
  if((computer->processes)->top == NULL)
    {
      return 0;
    };

  Node *temp = (computer->processes)->top;
  int sum = 0;
  while(temp != NULL)
    {
      sum = sum + (temp->data)->requiredMemory;
      temp = temp->next;
    };
  return sum;
}



int addProgram(Computer computer, Program program)
{
  int used = usedMemory(computer);
  if( ( used + (program->requiredMemory) ) > (computer->memoryCapacity) )
    {
      printf("Can not add this program - full of memory \n");
      return 0;
    }
  else
    {
      program->runningStatus = WAITING;
      enqueue(computer->processes, program);
      return 1;
    };
}


void checkProcesses(Computer computer)
{

  int i;
  Node *temp = (computer->processes)->top;
  for(i = 0; i < (computer->numberOfParallel) && temp != NULL; i++)
    {
      (temp->data)->runningStatus = RUNNING;
      temp = temp->next;
    };
}


Program killProgram(Computer computer)
{
  Program program = dequeue(computer->processes);
  checkProcesses(computer);
  return program;
}


void printStatus(Computer computer)
{
  if( isEmpty(computer->processes) )
    {
      printf("Empty processes\n");
      return;
    };

  Node *temp = (computer->processes)->top;

  while(temp != NULL)
    {
      printf(" %d - %d - %s\n", (temp->data)->programID, (temp->data)->requiredMemory, getStatus((temp->data)->runningStatus));
      temp = temp->next;
    };
}


char *getStatus(enum RunningStatus runningStatus)
{
  switch(runningStatus) {
  case NONE: return "NONE";
  case RUNNING: return "RUNNING";
  case WAITING: return "WAITING";
  default: return NULL;
  };
  return NULL;
}




  
  
