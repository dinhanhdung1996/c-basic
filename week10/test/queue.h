//#include "service.h"

//typedef Program ElType;

enum RunningStatus{
  NONE,
  RUNNING,
  WAITING };


typedef struct Program
{
  int programID;
  int requiredMemory;
  enum RunningStatus runningStatus;
}*Program;


typedef Program ElType;

typedef struct Node
{

  ElType data;
  struct Node *next;
}Node;


typedef struct Queue
 {
   Node *top;
}Queue;

typedef struct Computer
{
  Queue *processes;
  int memoryCapacity;
  int numberOfParallel;
}*Computer;



Queue *makeQueue();

Node *makeNode(ElType data);

void enqueue(Queue *queue, ElType data);

ElType dequeue(Queue *queue);

int isEmpty(Queue *queue);

  

Computer makeComputer(int memoryCapacity, int numberOfParallel);

Program makeProgram(int programID, int requiredMemory);

int parallelProgram(Computer computer);

int usedMemory(Computer computer);

int addProgram(Computer computer, Program program);

void checkProcesses(Computer computer);

Program killProgram(Computer computer);

void printStatus(Computer computer);

char *getStatus(enum RunningStatus runningStatus);


  
