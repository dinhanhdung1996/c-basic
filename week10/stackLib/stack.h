#ifndef STACK_H_INCLUDED
#define STACK_H_INCLUDED

typedef int ElType;

typedef struct Node{

    ElType data;
    struct Node *next;
}Node;


typedef struct Stack{

    Node *top;
}Stack;
Node *makeNode(ElType data);
Stack *makeStack();
void push(ElType data, Stack *stack);
ElType pop(Stack * stack);
ElType viewTop(Stack *stack);

#endif // STACK_H_INCLUDED
