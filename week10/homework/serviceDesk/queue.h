
typedef float ElType;



typedef struct Node
{

  ElType data;
  struct Node *next;
}Node;


typedef struct Queue
 {
   Node *top;
}Queue;




Queue *makeQueue();

Node *makeNode(ElType data);

void enqueue(Queue *queue, ElType data);

ElType dequeue(Queue *queue);

int isEmpty(Queue *queue);

  
  
