#include <stdio.h>
#include <stdlib.h>
#include "queue.h"



int countPeople(Queue *queue)
{
  if(queue->top == NULL)
    {
      return 0;
    };

  Node *temp = queue->top;

  int count = 0;
  while(temp != NULL)
    {
      count++;
      temp = temp->next;
    };

  return count;
}


int waitTime(Queue *queue)
{
  return countPeople(queue) * 15;
}

Queue *minQueue(Queue **queue, int n)
{
  int i, minIndex;
  minIndex = 0;
  int min = waitTime(queue[0]);
  for(i = 0; i< n; i++)
    {
      if(waitTime(queue[i]) < min)
	{

	  min = waitTime(queue[i]);
	  minIndex = i;
	};
    };

  return queue[minIndex];
}


int main()
{
  float lastTime =0 ;
  printf("INput the number of service desk: ");
  int num;
  scanf("%d", &num);

  Queue **queue = (Queue **)malloc(sizeof(Queue *) * num);

  int i;
  for(i = 0; i< num; i++)
    {
      queue[i] = malloc(sizeof(Queue) );
    };

  int choice;

  do
    {
      printf("Menu \n");
      printf("1. Add people to Queue \n");
      printf("2. Statistic \n");
      printf("Your choice: ");
      scanf("%d", &choice);

      while(getchar() != '\n');


      if(choice == 1)
	{
	  float time;
	  do
	    {
	      printf("Input the time for the next person : ");
	      scanf("%f", &time);
	      while(getchar() != '\n');
	      //if(time > lastTime) lastTime = time;
	    }
	  while(time  < lastTime);
	  lastTime = time;
	  Queue *temp = minQueue(queue, num);

	  enqueue(temp, time);
	};

      if(choice == 2)
	{

	  for(i = 0; i< num; i++)
	    {
	      printf("Queue %d\n", i+ 1);
	      printf("Number of people waiting: %d - ", countPeople(queue[i]));
	      printf("Wait time: %d minutes\n", waitTime(queue[i]));
	    };
	};

    }
  while(choice != 3);

  return 0;
}
	      
