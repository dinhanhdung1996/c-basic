
typedef int ElType;



typedef struct Node
{

  ElType data;
  struct Node *next;
}Node;


typedef struct Queue
 {
   Node *top;
}Queue;




Queue *makeQueue();

Node *makeNode(ElType data);

void enqueue(ElType data, Queue *queue);

ElType dequeue(Queue *queue);

int isEmpty(Queue *queue);

  
  
