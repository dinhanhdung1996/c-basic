#ifndef _DATATYPE_H
#define _DATATYPE_H




typedef struct phoneAddress{
  char tel[40];
  char name[40];
  char email[40];
}phoneAddress;

typedef struct Satan{
  int ID;
  char toy[20];
  int num;
}Satan;

typedef Satan ElType;

typedef struct Node{
  ElType key;
  struct Node *left, *right;
}Node;


typedef Node *Tree;

typedef struct NodeList{
    ElType data;
    struct NodeList *next;
}NodeList;


typedef struct Stack{
	NodeList *top;
}Stack;

typedef struct Queue{
  NodeList *top;
}Queue;

#endif
