#include <stdio.h>
#include <stdlib.h>
#include "dataType.h"
#include "phoneDeal.h"
#include "stack.h"


int main()
{
  int choice;
  Stack *stack = makeStack();
  phoneAddress *phone = (phoneAddress *)malloc(sizeof(phoneAddress) * 100);

  do
    {
      printf("MENU\n");
      printf("1. Get Data\n");
      printf("2. Push data to stack\n");
      printf("3. Pop data from stack\n");
      printf("4. View Top\n");
      printf("Your choie: ");
      scanf("%d", &choice);
      while(getchar() != '\n');


      if(choice == 1)
	{
	  int num = getData("phoneBook.txt", phone);

	  int i;
	  for(i = 0; i< num; i++)
	    {
	      push(phone[i], stack);
	    };

	};

      if(choice == 2)
	{

	  phoneAddress newPhone;
	  printf("Input name : ");
	  gets(newPhone.name);
	  printf("Input telephone: ");
	  gets(newPhone.tel);
	  printf("Input email: ");
	  gets(newPhone.email);

	  push(newPhone, stack);

	};


      if(choice == 3)
	{
	  phoneAddress newPhone = pop(stack, &nullPhone);
	  printPhone(newPhone);
	};


      if(choice == 4)
	{
	  phoneAddress newPhone = viewTop(stack, &nullPhone);
	  printPhone(newPhone);

	};

    }while(choice != 5);

  return 0;
}
