#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dataType.h"
#include "Satan.h"
#include "BinarySearchTree.h"



int main()
{
  Tree tree = makeNullTree();
  Satan *satan = (Satan *)malloc(sizeof(Satan) * 100);

  int choice;
  do
    {
      printf("MENU \n");
      printf("1. File A\n");
      printf("2. File B\n");
      printf("3. Search Fault\n");
      printf("4. Merge\n");
      printf("5. Statistic\n");


      printf("Your choice: ");
      scanf("%d", &choice);

      while(getchar() != '\n');


      if(choice == 1)
	{
	  getTreeData(&tree);
	  inOrderTraversal(tree, &printSatan);
	};

      if(choice == 2)
	{
	  getListData(satan);
	  printListSatan(satan);
	};
      if(choice == 3)
	{
	  searchFault(&tree, satan);
	  inOrderTraversal(tree, &printSatan);
	};

      if(choice == 4)
	{
	  merge(&tree, satan);
	  inOrderTraversal(tree, &printSatan);
	};

      if(choice == 5)
	{
	  inOrderTraversal(tree, &printSatan);
	  Tree newTree = statistic(tree);
	  inOrderTraversal(newTree, &printNum);
	};

      
    }while(choice != 6);
}
