#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dataType.h"
#include "BinarySearchTree.h"
#include "Satan.h"
#define A "A.txt"
#define B "B.txt"


int IDCompare(Satan a, Satan b)
{
  if( a.ID > b.ID) return 1;
  else if(a.ID < b.ID) return -1;
  else return 0;
}


void getTreeData(Tree *tree)
{
  FILE * f=  fopen(A, "r");
  if(f == NULL)
    {
      printf("file unavailable\n");
      return;
    };


  char *buffer = (char*)malloc(sizeof(char) * 100);
  char *token = (char*)malloc(sizeof(char) * 100);
  char *s = "\n\t";

  Satan satan;
  while(fgets(buffer, 100, f) != NULL)
    {
      satan.num = 0;
      token = strtok(buffer, s);
      satan.ID = atoi(token);
      token = strtok(NULL, s);
      if(token == NULL){
	//strcpy(satan.toy, "");
	printf("this line lack of information do you want to input manually?\n");
	printf("1. yes\n");
	printf("2. no\n");
	int choice;
	printf("Your choice: ");
	scanf("%d", &choice);
	while(getchar() != '\n');

	if(choice == 1)
	  {
	    printf(" ID: %d\n", satan.ID);
	    printf("Input the toy into list (no space)\n");
	    char *toy=  (char *)malloc(sizeof(char) * 100);
	    gets(toy);
	    strcpy(satan.toy, toy);
	  }
	else strcpy(satan.toy, "");
      }
      else strcpy(satan.toy, token);
      insertNode(tree, satan, &IDCompare);
    };
  fclose(f);
}

Satan makeNullSatan(){
  Satan satan;
  satan.ID = -1;
  strcpy(satan.toy, "NULL");
  return satan;
}


void getListData(Satan *satan)
{
  FILE * f = fopen(B, "r");
  if(f == NULL)
    {
      printf("File unavailable\n");
      return;
    };


  char *buffer = (char *)malloc(sizeof(char) * 100);
  char *token = (char *)malloc(sizeof(char) * 100);
  char *s = "\n\t";
  int i = 0;

  while(fgets(buffer, 100, f) != NULL)
    {
      token = strtok(buffer, s);
      satan[i].ID = atoi(token);
      token = strtok(NULL, s);
      if(token == NULL) strcpy(satan[i].toy, "");
      else strcpy(satan[i].toy, token);
      satan[i].num = 0;
      i++;
    };
  satan[i] = makeNullSatan();
}

void printSatan(Tree tree)
{
  printf("%d\t%s\n", (tree->key).ID, (tree->key).toy);
}


void printListSatan(Satan *satan)
{
  if(satan == NULL) return;
  int i;
  while( satan[i].ID > 0)
    {
      printf("%d\t%s\n", satan[i].ID, satan[i].toy);
      i++;
    };
}

void printElement(Satan satan)
{
  printf("%d\t%s\n", satan.ID, satan.toy);
}

void searchFault(Tree *tree, Satan *satan)
{
  if(satan == NULL) return;

  int i=0;
  while( satan[i].ID > 0)
    {
      Tree newNode = searchNode(*tree, satan[i], &IDCompare);
      if(newNode == NULL){
	i++;
	continue;
      };
      printf("delete node: ");
      printSatan(newNode);
      deleteNode(satan[i], tree, &IDCompare);
      i++;
    };
}

void merge(Tree *tree, Satan *satan)
{
  if(satan == NULL)
    {
      printf("Fault\n");
      return;
    };
  int i = 0;
  while(satan[i].ID > 0)
    {
      insertNode(tree, satan[i], &IDCompare);
      i++;
    };
}


int toyCompare(Satan satan1, Satan satan2)
{
  return (int)strcmp(satan1.toy, satan2.toy);
}


void pushTreeToTree(Tree tree1, Tree *tree2)
{
  if(strcmp((tree1->key).toy, "") != 0)insertNode(tree2, tree1->key, &toyCompare);
  if(tree1->left != NULL) pushTreeToTree(tree1->left, tree2);
  if(tree1->right != NULL)pushTreeToTree(tree1->right, tree2);
}

void count(Tree tree, Tree test)
{
  if(toyCompare(tree->key, test->key) == 0) (tree->key).num++;
  if(test->left != NULL) count(tree, test->left);
  if(test->right != NULL) count(tree, test->right);
}
void preOrderCheck(Tree tree, Tree test)
{
  count(tree, test);
  if(tree->left != NULL) preOrderCheck(tree->left, test);
  if(tree->right != NULL) preOrderCheck(tree->right, test);
}


Tree statistic(Tree tree)
{
  Tree tree2 = makeNullTree();
  pushTreeToTree(tree, &tree2);

  preOrderCheck(tree2, tree);
  return tree2;
}


void printNum(Tree tree)
{
  printf("%s - %d\n", (tree->key).toy, (tree->key).num);
}

  
  
  
